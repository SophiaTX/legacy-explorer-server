# SophiaTX Legacy Blockchain Explorer

**The support for this project ended on May 31st, 2018.**

**This software is NOT compatible with the production version of SophiaTX 
blockchain!**

This repository is intended for developers who are working on the software
based around the legacy blockchain. If you find any bugs, feel free to fix
them and open merge request.

---

This software is responsible for collecting blockchain data, creating useful
structures and providing APIs to easily query these in real time.

It's written in Spring Boot framework and requires Java 8+ and SophiaTX node to 
operate. The node can be deployed using the installer, just don't forget to 
adjust the node configuration before running the installer.

It is recommended to use locally installed node to speed up the initial import.

## API

Data can be retrieved using REST-ful APIs.  
Please refer to [API documentation](https://app.swaggerhub.com/apis/SophiaTX/sophiatx-legacy-explorer).

Web socket connection based on STOMP protocol is available to stream blocks.

## Production deployment

This software includes interactive CLI installer.  
Please refer to [Deployment process](/docker/production/README.md)

## Development container

Please refer to [Development container](/docker/development/README.md)

