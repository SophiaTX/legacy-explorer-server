package com.sophiatx.explorer;

import com.sophiatx.explorer.configuration.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class Application {
    public static final String VERSION = "v0.5.1";

    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Autowired
    public ApplicationConfiguration appConfig;

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> {
            String nodeAddress = appConfig.getNode().getFullAddress(false);
            logger.info("Using SophiaTX blockchain node " + nodeAddress);
        };
    }
}
