package com.sophiatx.explorer.lang;

public class ApiDoc {
    public static final String LIMIT = "The numbers of items to return";
    public static final String OFFSET = "The number of items to skip before starting to collect the result set";
    public static final String SORT_DIR = "The direction in which to sort the dataset";
    public static final String SORT_COL = "The field to sort the dataset by";
}
