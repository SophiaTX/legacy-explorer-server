package com.sophiatx.explorer.event;

import com.sophiatx.explorer.database.model.Block;

public class NewBlockReceivedEvent {
    private Block block;

    public NewBlockReceivedEvent(Block block) {
        this.block = block;
    }

    public Block getBlock() {
        return block;
    }
}
