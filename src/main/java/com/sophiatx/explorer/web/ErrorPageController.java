package com.sophiatx.explorer.web;

import com.sophiatx.explorer.api.resource.ErrorResource;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * This controller is used to replace the default whitelabel error page.
 * As all the logic exceptions are handled by the API's own ExceptionTranslator
 * or the advices, the only errors that arrive here are HTTP error codes like
 * 404 for non-existing files or non-existing API endpoints - the other 404s
 * are handled on the client-side by the way.
 */
@Controller
@RequestMapping(produces = {
        MediaType.APPLICATION_XML_VALUE,
        MediaType.APPLICATION_JSON_VALUE
})
public class ErrorPageController implements ErrorController {
    /**
     * @param request HttpServletRequest
     * @return Response Entity with the error
     */
    @RequestMapping("/error")
    public ResponseEntity<ErrorResource> handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Integer statusCode = Integer.valueOf(status.toString());

        return ResponseEntity.status(statusCode).body(new ErrorResource(
                "HttpError",
                HttpStatus.resolve(statusCode).getReasonPhrase(),
                statusCode
        ));
    }

    /**
     * @return Mapping to "/error"
     */
    @Override
    public String getErrorPath() {
        return "/error";
    }
}
