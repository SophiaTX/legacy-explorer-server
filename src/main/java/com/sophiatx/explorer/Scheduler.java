package com.sophiatx.explorer;

import com.sophiatx.explorer.blockchain.updater.AccountDatabaseUpdater;
import com.sophiatx.explorer.blockchain.updater.BlockDatabaseUpdater;
import com.sophiatx.explorer.blockchain.updater.MinerDatabaseUpdater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {
    private BlockDatabaseUpdater blockDatabaseUpdater;
    private AccountDatabaseUpdater accountDatabaseUpdater;
    private MinerDatabaseUpdater minerDatabaseUpdater;

    @Autowired
    public Scheduler(
            BlockDatabaseUpdater blockDatabaseUpdater,
            AccountDatabaseUpdater accountDatabaseUpdater,
            MinerDatabaseUpdater minerDatabaseUpdater
    ) {
        this.blockDatabaseUpdater = blockDatabaseUpdater;
        this.accountDatabaseUpdater = accountDatabaseUpdater;
        this.minerDatabaseUpdater = minerDatabaseUpdater;
    }

    @Scheduled(fixedDelay = 5000)
    public void fetchMissingObjects() {
        this.accountDatabaseUpdater.fetchMissingAccounts();
        this.minerDatabaseUpdater.fetchMissingMiners();
        this.blockDatabaseUpdater.fetchMissingBlocks();
    }

    @Scheduled(fixedDelay = 60000)
    public void updateExistingObjects() {
        this.accountDatabaseUpdater.updateBalances();
        this.minerDatabaseUpdater.updateExistingMiners();
    }
}
