package com.sophiatx.explorer.listener;

import com.sophiatx.explorer.api.resource.BlockResource;
import com.sophiatx.explorer.event.NewBlockReceivedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class BroadcastBlockUpdates {
    private SimpMessagingTemplate messaging;

    @Autowired
    public BroadcastBlockUpdates(SimpMessagingTemplate simpMessagingTemplate) {
        this.messaging = simpMessagingTemplate;
    }

    @EventListener
    public void handle(NewBlockReceivedEvent event) {
        messaging.convertAndSend(
                "/stream/blocks",
                new BlockResource(event.getBlock())
        );
    }
}
