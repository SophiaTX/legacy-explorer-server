package com.sophiatx.explorer.helper;

public class IdConverter {
    public static Integer blockchainToInt(String blockchainId) throws IllegalArgumentException {
        String id;

        try {
            id = blockchainId.split("\\.")[2];
        } catch (Exception e) {
            throw new IllegalArgumentException("Provided string is not a blockchain id!");
        }

        return Integer.parseInt(id);
    }
}
