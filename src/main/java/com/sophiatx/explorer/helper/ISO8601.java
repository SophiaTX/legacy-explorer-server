package com.sophiatx.explorer.helper;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class ISO8601 {
    private static DateTimeFormatter df
            = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    public static LocalDateTime toDate(String dateString)
            throws DateTimeParseException {
        return LocalDateTime.parse(dateString, df);
    }

    public static String toString(LocalDateTime date) {
        return df.format(date);
    }
}
