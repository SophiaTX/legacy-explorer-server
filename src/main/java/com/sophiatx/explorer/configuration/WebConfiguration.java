package com.sophiatx.explorer.configuration;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    private static final String API = RestConfiguration.API_ENDPOINT;

    /**
     * Disable path suffix matching ( file type extensions )
     * TODO: this does not work
     */
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(false);
        configurer.setUseRegisteredSuffixPatternMatch(false);
    }

    /**
     * Configure default content type to JSON
     */
    @Override
    public void configureContentNegotiation(
            ContentNegotiationConfigurer configurer
    ) {
        configurer.defaultContentType(MediaType.APPLICATION_JSON);
    }

    /**
     * Adjust request mapping for API routes
     *
     * @return Web MVC registrations w/ adjusted request mapping
     */
    @Bean
    public WebMvcRegistrations webMvcRegistrationsHandlerMapping() {
        return new WebMvcRegistrations() {
            @Override
            public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
                return getAdjustedRequestMappingHandler();
            }
        };
    }

    /**
     * This custom request mapping handler prefixes mappings declared in
     * {@link RestController} annotated classes with API_ENDPOINT constant.
     * This effectively adds "api/" in front of every REST endpoint path.
     *
     * @return Request mapping handler that will prefix RestControllers
     */
    private RequestMappingHandlerMapping getAdjustedRequestMappingHandler() {
        return new RequestMappingHandlerMapping() {
            @Override
            protected void registerHandlerMethod(
                    Object handler,
                    Method method,
                    RequestMappingInfo mapping
            ) {
                RestController restAnnotation = method.getDeclaringClass()
                        .getAnnotation(RestController.class);

                // if the declaring controller has @RestController annotation
                if (restAnnotation != null) {
                    PatternsRequestCondition apiPattern
                            = new PatternsRequestCondition(API)
                            .combine(mapping.getPatternsCondition());

                    mapping = new RequestMappingInfo(
                            mapping.getName(),
                            apiPattern,
                            mapping.getMethodsCondition(),
                            mapping.getParamsCondition(),
                            mapping.getHeadersCondition(),
                            mapping.getConsumesCondition(),
                            mapping.getProducesCondition(),
                            mapping.getCustomCondition()
                    );
                }

                super.registerHandlerMethod(handler, method, mapping);
            }
        };
    }

    /**
     * This method is responsible for forwarding everything but files
     * ( paths ending with .something ) to index. This is required for the
     * Angular ( static / web app ) to be served properly by the Spring.
     * The actual URL processing and view rendering is done on the client-side.
     * The only URLs Spring has to process are API endpoints (/api, /stomp) and
     * static files ( for example: the web app, images, ... ).
     */
    public void addViewControllers(ViewControllerRegistry registry) {
        String[] excludedPaths = new String[]{
                RestConfiguration.API_ENDPOINT,
                WebSocketConfiguration.STOMP_ENDPOINT
        };
        String e = "(?!" + String.join("|", excludedPaths) + ")";

        registry.addViewController("/{x:" + e + "\\w+}")
                .setViewName("forward:/");
        registry.addViewController("/{x:" + e + "**/{y:\\w+}")
                .setViewName("forward:/");
        registry.addViewController("/{x:" + e + "\\w+}/**{y:[^\\.]+$}")
                .setViewName("forward:/");
    }
}
