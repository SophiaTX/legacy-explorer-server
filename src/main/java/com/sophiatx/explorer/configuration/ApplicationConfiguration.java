package com.sophiatx.explorer.configuration;

import com.sophiatx.explorer.configuration.properties.NodeConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties({NodeConfigurationProperties.class})
public class ApplicationConfiguration {
    @Autowired
    private NodeConfigurationProperties nodeConfigurationProperties;

    public NodeConfigurationProperties getNode() {
        return nodeConfigurationProperties;
    }
}
