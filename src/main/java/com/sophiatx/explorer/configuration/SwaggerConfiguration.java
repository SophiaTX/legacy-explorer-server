package com.sophiatx.explorer.configuration;

import com.fasterxml.classmate.TypeResolver;
import com.sophiatx.explorer.Application;
import com.sophiatx.explorer.api.resource.ErrorResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {
    /**
     * API title / name
     */
    private static final String TITLE = "SophiaTX Legacy Blockchain Explorer";

    /**
     * API description
     */
    private static final String DESCRIPTION = "";

    /**
     * Terms of Service
     */
    private static final String TOS_URL = "";

    /**
     * Contact to person maintaining the API
     */
    private static final Contact CONTACT = new Contact(
            /* name  */ "Patrik Gajdos",
            /* url   */ "",
            /* email */ "patrik.gajdos@sophiatx.com"
    );

    /**
     * Licensing information
     */
    private static final String LICENCE = "Apache 2.0";
    private static final String LICENCE_URL = "http://www.apache.org/licenses/LICENSE-2.0.txt";

    private final TypeResolver typeResolver;

    @Autowired
    public SwaggerConfiguration(TypeResolver typeResolver) {
        this.typeResolver = typeResolver;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.sophiatx.explorer.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .additionalModels(typeResolver.resolve(ErrorResource.class));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                TITLE,
                DESCRIPTION,
                Application.VERSION,
                TOS_URL,
                CONTACT,
                LICENCE,
                LICENCE_URL,
                Collections.emptyList()
        );
    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(false)
                .defaultModelRendering(ModelRendering.MODEL)
                .displayRequestDuration(true)
                .docExpansion(DocExpansion.LIST)
                .filter(false)
                .build();
    }
}
