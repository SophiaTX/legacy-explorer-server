package com.sophiatx.explorer.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.InetAddress;

@ConfigurationProperties(prefix = "explorer.node")
public class NodeConfigurationProperties {
    private InetAddress host;
    private Integer port;

    public InetAddress getHost() {
        return host;
    }

    public NodeConfigurationProperties setHost(InetAddress host) {
        this.host = host;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public NodeConfigurationProperties setPort(Integer port) {
        this.port = port;
        return this;
    }

    /**
     * Convenience method to convert InetAddress ( host ) and Integer ( port )
     * to a standard String representation ( [http://]host:port ).
     *
     * @param includeHttpPrefix Whether we should include the "http://" prefix
     * @return String representation of the address and port
     */
    public String getFullAddress(boolean includeHttpPrefix) {
        String address = "";

        if (includeHttpPrefix)
            address += "http://";

        address += this.host.getHostAddress() + ":" + this.port;

        return address;
    }
}
