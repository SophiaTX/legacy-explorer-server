package com.sophiatx.explorer.blockchain.enums;

public enum OperationType {
    TRANSFER,
    ACCOUNT_CREATE,
    ACCOUNT_UPDATE,
    ASSET_CREATE,
    ASSET_ISSUE,
    ASSET_PUBLISH_FEED,
    MINER_CREATE,
    MINER_UPDATE,
    MINER_UPDATE_GLOBAL_PARAMETERS,
    PROPOSAL_CREATE,
    PROPOSAL_UPDATE,
    PROPOSAL_DELETE,
    WITHDRAW_PERMISSION_CREATE,
    WITHDRAW_PERMISSION_UPDATE,
    WITHDRAW_PERMISSION_CLAIM,
    WITHDRAW_PERMISSION_DELETE,
    VESTING_BALANCE_CREATE,
    VESTING_BALANCE_WITHDRAW,
    CUSTOM,
    ASSERT,
    CONTENT_SUBMIT,
    REQUEST_TO_BUY,
    LEAVE_RATING_AND_COMMENT,
    READY_TO_PUBLISH,
    PROOF_OF_CUSTODY,
    DELIVER_KEYS,
    SUBSCRIBE,
    SUBSCRIBE_BY_AUTHOR,
    AUTOMATIC_RENEWAL_OF_SUBSCRIPTION,
    REPORT_STATS,
    SET_PUBLISHING_MANAGER,
    SET_PUBLISHING_RIGHT,
    CONTENT_CANCELLATION,
    ASSET_FUND_POOLS,
    ASSET_RESERVE,
    ASSET_CLAIM_FEES,
    UPDATE_USER_ISSUED_ASSET,
    UPDATE_MONITORED_ASSET,
    READY_TO_PUBLISH2,
    DISALLOW_AUTOMATIC_RENEWAL_OF_SUBSCRIPTION,
    RETURN_ESCROW_SUBMISSION,
    RETURN_ESCROW_BUYING,
    PAY_SEEDER,
    FINISH_BUYING,
    RENEWAL_OF_SUBSCRIPTION;

    public static OperationType fromInt(Integer operationId) {
        for (OperationType o : OperationType.values()) {
            if (o.ordinal() == operationId) {
                return o;
            }
        }
        return null;
    }

    private static final int size = OperationType.values().length;

    public static int size() {
        return size;
    }
}
