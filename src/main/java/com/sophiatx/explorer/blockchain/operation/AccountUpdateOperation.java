package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Update an existing account
 *
 * This operation is used to update an existing account. It can be used to
 * update the authorities, or adjust the options on the account.
 */
public class AccountUpdateOperation extends AbstractOperation {
    AccountUpdateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
