package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Publish price feeds for market-issued assets
 *
 * Price feed providers use this operation to publish their price feeds for
 * market-issued assets. A price feed is used to tune the market for a
 * particular market-issued asset. For each value in the feed, the median
 * across all committee_member feeds for that asset is calculated and the
 * market for the asset is configured with the median of that value.
 *
 * The feed in the operation contains three prices: a call price limit, a short
 * price limit, and a settlement price. The call limit price is structured as
 * (collateral asset) / (debt asset) and the short limit price is structured
 * as (asset for sale) / (collateral asset). Note that the asset IDs are
 * opposite to eachother, so if we're publishing a feed for USD, the call limit
 * price will be CORE/USD and the short limit price will be USD/CORE.
 * The settlement price may be flipped either direction, as long as it is a
 * ratio between the market-issued asset and its collateral.
 */
public class AssetPublishFeedOperation extends AbstractOperation {
    AssetPublishFeedOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("publisher");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
