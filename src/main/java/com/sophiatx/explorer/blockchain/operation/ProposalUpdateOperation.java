package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation allows accounts to add or revoke approval of a proposed
 * transaction. Signatures sufficient to satisfy the authority of each account
 * in approvals are required on the transaction containing this operation.
 *
 * If an account with a multi-signature authority is listed in approvals_to_add
 * or approvals_to_remove, either all required signatures to satisfy that
 * account's authority must be provided in the transaction containing this
 * operation, or a secondary proposal must be created which contains this
 * operation.
 *
 * NOTE: If the proposal requires only an account's active authority,
 * the account must not update adding its owner authority's approval.
 * This is considered an error. An owner approval may only be added if
 * the proposal requires the owner's authority.
 *
 * If an account's owner and active authority are both required, only
 * the owner authority may approve. An attempt to add or remove active
 * authority approval to such a proposal will fail.
 */
public class ProposalUpdateOperation extends AbstractOperation {
    ProposalUpdateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("fee_paying_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
