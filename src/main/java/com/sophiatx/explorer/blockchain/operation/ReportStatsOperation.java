package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation is used to report stats.
 * These stats are later used to rate seeders.
 */
public class ReportStatsOperation extends AbstractOperation {
    ReportStatsOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("consumer");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
