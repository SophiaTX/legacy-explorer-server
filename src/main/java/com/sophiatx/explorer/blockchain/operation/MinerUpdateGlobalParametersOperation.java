package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used by miners to update the global parameters of the blockchain.
 *
 * This operation allows the miners to update the global parameters on the
 * blockchain. These control various tunable aspects of the chain, including
 * block and maintenance intervals, maximum data sizes, the fees charged by
 * the network, etc.
 *
 * This operation may only be used in a proposed transaction, and a proposed
 * transaction which contains this operation must have a review period specified
 * in the current global parameters before it may be accepted.
 */
public class MinerUpdateGlobalParametersOperation extends AbstractOperation {
    MinerUpdateGlobalParametersOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("payer");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
