package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Creates a subscription to author. This function is used by consumers.
 */
public class SubscribeOperation extends AbstractOperation {
    SubscribeOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("from");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("to");
    }
}
