package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This virtual operation disallows automatic renewal of subscription if
 * consumer doesn't have enought balance to renew expired subscription
 */
public class DisallowAutomaticRenewalOfSubscriptionOperation extends AbstractOperation {
    DisallowAutomaticRenewalOfSubscriptionOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("consumer");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        // TODO: somehow get to target ID via subscription_id_type subscription
        return null;
    }
}
