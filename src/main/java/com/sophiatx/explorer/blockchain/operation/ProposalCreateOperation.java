package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Creates a transaction proposal, for use in multi-sig scenarios
 *
 * Creates a transaction proposal. The operations which compose the transaction
 * are listed in order in proposed_ops, and expiration_time specifies the time
 * by which the proposal must be accepted or it will fail permanently. The
 * expiration_time cannot be farther in the future than the maximum expiration
 * time set in the global properties object.
 */
public class ProposalCreateOperation extends AbstractOperation {
    ProposalCreateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("fee_paying_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
