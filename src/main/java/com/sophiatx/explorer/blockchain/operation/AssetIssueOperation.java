package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

public class AssetIssueOperation extends AbstractOperation {
    AssetIssueOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("issuer");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("issue_to_account");
    }
}
