package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Seeders have to periodically prove that they hold the content.
 */
public class ProofOfCustodyOperation extends AbstractOperation {
    ProofOfCustodyOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("seeder");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
