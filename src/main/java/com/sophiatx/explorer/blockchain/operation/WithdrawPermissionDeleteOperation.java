package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation cancels a withdrawal permission,
 * thus preventing any future withdrawals using that permission.
 */
public class WithdrawPermissionDeleteOperation extends AbstractOperation {
    WithdrawPermissionDeleteOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("withdraw_from_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("authorized_account");
    }
}
