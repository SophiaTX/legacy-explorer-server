package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This oeration is used to update the settings for an existing withdrawal
 * permission. The accounts to withdraw to and from may never be updated.
 * The fields which may be updated are the withdrawal limit (both amount and
 * asset type may be updated), the withdrawal period length, the remaining
 * number of periods until expiration, and the starting time of the new period.
 */
public class WithdrawPermissionUpdateOperation extends AbstractOperation {
    WithdrawPermissionUpdateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("withdraw_from_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("authorized_account");
    }
}
