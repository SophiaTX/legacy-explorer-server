package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation creates a withdrawal permission, which allows some authorized
 * account to withdraw from an authorizing account. This operation is primarily
 * useful for scheduling recurring payments.
 *
 * Withdrawal permissions define withdrawal periods, which is a span of time
 * during which the authorized account may make a withdrawal. Any number of
 * withdrawals may be made so long as the total amount withdrawn per period
 * does not exceed the limit for any given period.
 *
 * Withdrawal permissions authorize only a specific pairing, i.e. a permission
 * only authorizes one specified authorized account to withdraw from one
 * specified authorizing account. Withdrawals are limited and may not exceet
 * the withdrawal limit. The withdrawal must be made in the same asset as the
 * limit; attempts with withdraw any other asset type will be rejected.
 */
public class WithdrawPermissionCreateOperation extends AbstractOperation {
    WithdrawPermissionCreateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("withdraw_from_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("authorized_account");
    }
}
