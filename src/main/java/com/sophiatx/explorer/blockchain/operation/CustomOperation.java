package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.helper.HexAscii;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomOperation extends AbstractOperation {
    CustomOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected JSONObject processData(JSONObject data) {
        // decode custom data ( HEX to ASCII )
        data.put("data", new JSONObject(
                HexAscii.hexToAscii(data.getString("data"))
        ));

        try {
            JSONObject customData = data.getJSONObject("data");

            // custom payload is quite often stored as JSON, it makes sense
            // to try to convert it to an object representation
            JSONObject customPayload = new JSONObject(
                    customData.getString("Data")
            );

            // if we succeeded ( it was a JSON, replace string with object )
            customData.put("Data", customPayload);

            // now replace the whole data object in the operation
            data.put("data", customData);
        } catch (JSONException e) {
            // we can safely ignore this failure, data just wasn't a json...
        }

        return data;
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getJSONObject("data").getString("Sender");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return data.getJSONObject("data").getString("Receiver");
    }
}
