package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Used to allow/disallow automatic renewal of expired subscription.
 */
public class AutomaticRenewalOfSubscriptionOperation extends AbstractOperation {
    AutomaticRenewalOfSubscriptionOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("consumer");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        // TODO: somehow get to target ID via subscription_id_type subscription
        return null;
    }
}
