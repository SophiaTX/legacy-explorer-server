package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.id.OperationId;
import com.sophiatx.explorer.database.model.Account;
import com.sophiatx.explorer.database.model.Operation;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.helper.IdConverter;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public abstract class AbstractOperation {
    private OperationType type;
    private JSONObject data;
    private Account sender;
    private Account target;
    private AccountRepository accountRepository;
    protected static final Logger logger
            = LoggerFactory.getLogger(AbstractOperation.class);

    AbstractOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        this.data = processData(data);
        this.type = type;
        this.accountRepository = accountRepository;

        this.sender = processSender(this.data, this.accountRepository);
        this.target = processTarget(this.data, this.accountRepository);
    }

    /**
     * When the data object needs some individual processing before any
     * operations are done on top of it, override this method.
     */
    protected JSONObject processData(JSONObject data) {
        return data;
    }

    /**
     * This method should obtain sender's account ID and look it up in the DB
     * Override if more complex logic is needed.
     *
     * @param data              Processed data from blockchain
     * @param accountRepository Account repository
     * @return Sender account object
     */
    protected Account processSender(
            JSONObject data,
            AccountRepository accountRepository
    ) {
        String senderId;

        try {
            senderId = parseSenderAccountId(data);
        } catch (JSONException e) {
            logger.warn("Failed to parse sender account ID!");
            return null;
        }

        return getAccountByBlockchainId(senderId);
    }

    /**
     * This method should obtain target's account ID and look it up in the DB
     * Override if more complex logic is needed.
     *
     * @param data              Processed data from blockchain
     * @param accountRepository Account repository
     * @return Target account object
     */
    protected Account processTarget(
            JSONObject data,
            AccountRepository accountRepository
    ) {
        String targetId;

        try {
            targetId = parseTargetAccountId(data);
        } catch (JSONException e) {
            logger.warn("Failed to parse target account ID!");
            return null;
        }

        return getAccountByBlockchainId(targetId);
    }

    /**
     * Obtain sender's account ID ( blockchain format ) from processed data
     *
     * @param data Processed data from blockchain
     * @return Sender account ID in blockchain format
     * @throws JSONException When parsing unexpectedly fails ( invalid data )
     */
    protected abstract String parseSenderAccountId(
            JSONObject data
    ) throws JSONException;

    /**
     * Obtain target's account ID ( blockchain format ) from processed data
     *
     * @param data Processed data from blockchain
     * @return Target account ID in blockchain format
     * @throws JSONException When parsing unexpectedly fails ( invalid data )
     */
    protected abstract String parseTargetAccountId(
            JSONObject data
    ) throws JSONException;

    /**
     * Get account object by its blockchain ID
     * TODO: move this to its own helper class
     *
     * @param blockchainId Account ID in blockchain format ( 1.2.x )
     * @return Account
     */
    protected Account getAccountByBlockchainId(String blockchainId) {
        Integer accountId;

        try {
            accountId = IdConverter.blockchainToInt(blockchainId);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return accountRepository
                .findById(accountId)
                .orElse(null);
    }

    // getters

    public OperationType getType() {
        return type;
    }

    public JSONObject getData() {
        return data;
    }

    public Optional<Account> getSender() {
        return Optional.ofNullable(sender);
    }

    public Optional<Account> getTarget() {
        return Optional.ofNullable(target);
    }

    public Operation getAsModel() {
        return new Operation(
                new OperationId(),
                getType(),
                getData(),
                getSender().orElse(null),
                getTarget().orElse(null)
        );
    }
}
