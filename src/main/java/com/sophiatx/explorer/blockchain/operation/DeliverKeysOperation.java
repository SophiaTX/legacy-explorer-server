package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation is used to send encrypted share of a content and proof of
 * delivery to consumer.
 */
public class DeliverKeysOperation extends AbstractOperation {
    DeliverKeysOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("seeder");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        // TODO: somehow get consumer ID
        return null;
    }
}
