package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * The chain allows a user to create a vesting balance.
 * Normally, vesting balances are created automatically as part
 * of cashback and worker operations. This operation allows
 * vesting balances to be created manually as well.
 *
 * Manual creation of vesting balances can be used by a stakeholder
 * to publicly demonstrate that they are committed to the chain.
 * It can also be used as a building block to create transactions
 * that function like public debt. Finally, it is useful for
 * testing vesting balance functionality.
 */
public class VestingBalanceCreateOperation extends AbstractOperation {
    VestingBalanceCreateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("creator");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("owner");
    }
}
