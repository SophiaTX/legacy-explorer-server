package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.model.Account;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

public class AccountCreateOperation extends AbstractOperation {
    public AccountCreateOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("registrar");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        // we will override standard process below to save some DB queries
        return null;
    }

    @Override
    protected Account processTarget(
            JSONObject data,
            AccountRepository accountRepository
    ) {
        String accountName;

        try {
            accountName = data.getString("name");
        } catch (JSONException e) {
            return null;
        }

        return accountRepository
                .findByName(accountName)
                .orElse(null);
    }
}
