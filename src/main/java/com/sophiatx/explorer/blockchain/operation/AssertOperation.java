package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation performs no changes to the database state, but can but used
 * to verify pre or post conditions for other operations.
 */
public class AssertOperation extends AbstractOperation {
    AssertOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("fee_paying_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
