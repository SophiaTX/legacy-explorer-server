// THIS FILE WAS AUTO-GENERATED, DO NOT MODIFY IT, IT WILL BE OVERRIDDEN!
// MODIFY GENERATION SCRIPTS IN `explorer-helper-scripts` REPOSITORY INSTEAD

package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AbstractOperationFactory {
    private AccountRepository accountRepository;

    public AbstractOperationFactory(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Optional<AbstractOperation> fromTypeAndData(
            Integer type,
            JSONObject data
    ) {
        OperationType t = OperationType.fromInt(type);
        if (t == null) {
            return Optional.empty();
        }

        AbstractOperation operation;

        switch (t) {
            case TRANSFER:
                operation = transfer(data);
                break;
            case ACCOUNT_CREATE:
                operation = accountCreate(data);
                break;
            case ACCOUNT_UPDATE:
                operation = accountUpdate(data);
                break;
            case ASSET_CREATE:
                operation = assetCreate(data);
                break;
            case ASSET_ISSUE:
                operation = assetIssue(data);
                break;
            case ASSET_PUBLISH_FEED:
                operation = assetPublishFeed(data);
                break;
            case MINER_CREATE:
                operation = minerCreate(data);
                break;
            case MINER_UPDATE:
                operation = minerUpdate(data);
                break;
            case MINER_UPDATE_GLOBAL_PARAMETERS:
                operation = minerUpdateGlobalParameters(data);
                break;
            case PROPOSAL_CREATE:
                operation = proposalCreate(data);
                break;
            case PROPOSAL_UPDATE:
                operation = proposalUpdate(data);
                break;
            case PROPOSAL_DELETE:
                operation = proposalDelete(data);
                break;
            case WITHDRAW_PERMISSION_CREATE:
                operation = withdrawPermissionCreate(data);
                break;
            case WITHDRAW_PERMISSION_UPDATE:
                operation = withdrawPermissionUpdate(data);
                break;
            case WITHDRAW_PERMISSION_CLAIM:
                operation = withdrawPermissionClaim(data);
                break;
            case WITHDRAW_PERMISSION_DELETE:
                operation = withdrawPermissionDelete(data);
                break;
            case VESTING_BALANCE_CREATE:
                operation = vestingBalanceCreate(data);
                break;
            case VESTING_BALANCE_WITHDRAW:
                operation = vestingBalanceWithdraw(data);
                break;
            case CUSTOM:
                operation = custom(data);
                break;
            case ASSERT:
                operation = _assert(data);
                break;
            case CONTENT_SUBMIT:
                operation = contentSubmit(data);
                break;
            case REQUEST_TO_BUY:
                operation = requestToBuy(data);
                break;
            case LEAVE_RATING_AND_COMMENT:
                operation = leaveRatingAndComment(data);
                break;
            case READY_TO_PUBLISH:
                operation = readyToPublish(data);
                break;
            case PROOF_OF_CUSTODY:
                operation = proofOfCustody(data);
                break;
            case DELIVER_KEYS:
                operation = deliverKeys(data);
                break;
            case SUBSCRIBE:
                operation = subscribe(data);
                break;
            case SUBSCRIBE_BY_AUTHOR:
                operation = subscribeByAuthor(data);
                break;
            case AUTOMATIC_RENEWAL_OF_SUBSCRIPTION:
                operation = automaticRenewalOfSubscription(data);
                break;
            case REPORT_STATS:
                operation = reportStats(data);
                break;
            case SET_PUBLISHING_MANAGER:
                operation = setPublishingManager(data);
                break;
            case SET_PUBLISHING_RIGHT:
                operation = setPublishingRight(data);
                break;
            case CONTENT_CANCELLATION:
                operation = contentCancellation(data);
                break;
            case ASSET_FUND_POOLS:
                operation = assetFundPools(data);
                break;
            case ASSET_RESERVE:
                operation = assetReserve(data);
                break;
            case ASSET_CLAIM_FEES:
                operation = assetClaimFees(data);
                break;
            case UPDATE_USER_ISSUED_ASSET:
                operation = updateUserIssuedAsset(data);
                break;
            case UPDATE_MONITORED_ASSET:
                operation = updateMonitoredAsset(data);
                break;
            case READY_TO_PUBLISH2:
                operation = readyToPublish2(data);
                break;
            case DISALLOW_AUTOMATIC_RENEWAL_OF_SUBSCRIPTION:
                operation = disallowAutomaticRenewalOfSubscription(data);
                break;
            case RETURN_ESCROW_SUBMISSION:
                operation = returnEscrowSubmission(data);
                break;
            case RETURN_ESCROW_BUYING:
                operation = returnEscrowBuying(data);
                break;
            case PAY_SEEDER:
                operation = paySeeder(data);
                break;
            case FINISH_BUYING:
                operation = finishBuying(data);
                break;
            case RENEWAL_OF_SUBSCRIPTION:
                operation = renewalOfSubscription(data);
                break;
            default:
                return Optional.empty();
        }

        return Optional.of(operation);
    }

    public TransferOperation transfer(JSONObject data) {
        return new TransferOperation(
                OperationType.TRANSFER, data, accountRepository
        );
    }

    public AccountCreateOperation accountCreate(JSONObject data) {
        return new AccountCreateOperation(
                OperationType.ACCOUNT_CREATE, data, accountRepository
        );
    }

    public AccountUpdateOperation accountUpdate(JSONObject data) {
        return new AccountUpdateOperation(
                OperationType.ACCOUNT_UPDATE, data, accountRepository
        );
    }

    public AssetCreateOperation assetCreate(JSONObject data) {
        return new AssetCreateOperation(
                OperationType.ASSET_CREATE, data, accountRepository
        );
    }

    public AssetIssueOperation assetIssue(JSONObject data) {
        return new AssetIssueOperation(
                OperationType.ASSET_ISSUE, data, accountRepository
        );
    }

    public AssetPublishFeedOperation assetPublishFeed(JSONObject data) {
        return new AssetPublishFeedOperation(
                OperationType.ASSET_PUBLISH_FEED, data, accountRepository
        );
    }

    public MinerCreateOperation minerCreate(JSONObject data) {
        return new MinerCreateOperation(
                OperationType.MINER_CREATE, data, accountRepository
        );
    }

    public MinerUpdateOperation minerUpdate(JSONObject data) {
        return new MinerUpdateOperation(
                OperationType.MINER_UPDATE, data, accountRepository
        );
    }

    public MinerUpdateGlobalParametersOperation minerUpdateGlobalParameters(JSONObject data) {
        return new MinerUpdateGlobalParametersOperation(
                OperationType.MINER_UPDATE_GLOBAL_PARAMETERS, data, accountRepository
        );
    }

    public ProposalCreateOperation proposalCreate(JSONObject data) {
        return new ProposalCreateOperation(
                OperationType.PROPOSAL_CREATE, data, accountRepository
        );
    }

    public ProposalUpdateOperation proposalUpdate(JSONObject data) {
        return new ProposalUpdateOperation(
                OperationType.PROPOSAL_UPDATE, data, accountRepository
        );
    }

    public ProposalDeleteOperation proposalDelete(JSONObject data) {
        return new ProposalDeleteOperation(
                OperationType.PROPOSAL_DELETE, data, accountRepository
        );
    }

    public WithdrawPermissionCreateOperation withdrawPermissionCreate(JSONObject data) {
        return new WithdrawPermissionCreateOperation(
                OperationType.WITHDRAW_PERMISSION_CREATE, data, accountRepository
        );
    }

    public WithdrawPermissionUpdateOperation withdrawPermissionUpdate(JSONObject data) {
        return new WithdrawPermissionUpdateOperation(
                OperationType.WITHDRAW_PERMISSION_UPDATE, data, accountRepository
        );
    }

    public WithdrawPermissionClaimOperation withdrawPermissionClaim(JSONObject data) {
        return new WithdrawPermissionClaimOperation(
                OperationType.WITHDRAW_PERMISSION_CLAIM, data, accountRepository
        );
    }

    public WithdrawPermissionDeleteOperation withdrawPermissionDelete(JSONObject data) {
        return new WithdrawPermissionDeleteOperation(
                OperationType.WITHDRAW_PERMISSION_DELETE, data, accountRepository
        );
    }

    public VestingBalanceCreateOperation vestingBalanceCreate(JSONObject data) {
        return new VestingBalanceCreateOperation(
                OperationType.VESTING_BALANCE_CREATE, data, accountRepository
        );
    }

    public VestingBalanceWithdrawOperation vestingBalanceWithdraw(JSONObject data) {
        return new VestingBalanceWithdrawOperation(
                OperationType.VESTING_BALANCE_WITHDRAW, data, accountRepository
        );
    }

    public CustomOperation custom(JSONObject data) {
        return new CustomOperation(
                OperationType.CUSTOM, data, accountRepository
        );
    }

    public AssertOperation _assert(JSONObject data) {
        return new AssertOperation(
                OperationType.ASSERT, data, accountRepository
        );
    }

    public ContentSubmitOperation contentSubmit(JSONObject data) {
        return new ContentSubmitOperation(
                OperationType.CONTENT_SUBMIT, data, accountRepository
        );
    }

    public RequestToBuyOperation requestToBuy(JSONObject data) {
        return new RequestToBuyOperation(
                OperationType.REQUEST_TO_BUY, data, accountRepository
        );
    }

    public LeaveRatingAndCommentOperation leaveRatingAndComment(JSONObject data) {
        return new LeaveRatingAndCommentOperation(
                OperationType.LEAVE_RATING_AND_COMMENT, data, accountRepository
        );
    }

    public ReadyToPublishOperation readyToPublish(JSONObject data) {
        return new ReadyToPublishOperation(
                OperationType.READY_TO_PUBLISH, data, accountRepository
        );
    }

    public ProofOfCustodyOperation proofOfCustody(JSONObject data) {
        return new ProofOfCustodyOperation(
                OperationType.PROOF_OF_CUSTODY, data, accountRepository
        );
    }

    public DeliverKeysOperation deliverKeys(JSONObject data) {
        return new DeliverKeysOperation(
                OperationType.DELIVER_KEYS, data, accountRepository
        );
    }

    public SubscribeOperation subscribe(JSONObject data) {
        return new SubscribeOperation(
                OperationType.SUBSCRIBE, data, accountRepository
        );
    }

    public SubscribeByAuthorOperation subscribeByAuthor(JSONObject data) {
        return new SubscribeByAuthorOperation(
                OperationType.SUBSCRIBE_BY_AUTHOR, data, accountRepository
        );
    }

    public AutomaticRenewalOfSubscriptionOperation automaticRenewalOfSubscription(JSONObject data) {
        return new AutomaticRenewalOfSubscriptionOperation(
                OperationType.AUTOMATIC_RENEWAL_OF_SUBSCRIPTION, data, accountRepository
        );
    }

    public ReportStatsOperation reportStats(JSONObject data) {
        return new ReportStatsOperation(
                OperationType.REPORT_STATS, data, accountRepository
        );
    }

    public SetPublishingManagerOperation setPublishingManager(JSONObject data) {
        return new SetPublishingManagerOperation(
                OperationType.SET_PUBLISHING_MANAGER, data, accountRepository
        );
    }

    public SetPublishingRightOperation setPublishingRight(JSONObject data) {
        return new SetPublishingRightOperation(
                OperationType.SET_PUBLISHING_RIGHT, data, accountRepository
        );
    }

    public ContentCancellationOperation contentCancellation(JSONObject data) {
        return new ContentCancellationOperation(
                OperationType.CONTENT_CANCELLATION, data, accountRepository
        );
    }

    public AssetFundPoolsOperation assetFundPools(JSONObject data) {
        return new AssetFundPoolsOperation(
                OperationType.ASSET_FUND_POOLS, data, accountRepository
        );
    }

    public AssetReserveOperation assetReserve(JSONObject data) {
        return new AssetReserveOperation(
                OperationType.ASSET_RESERVE, data, accountRepository
        );
    }

    public AssetClaimFeesOperation assetClaimFees(JSONObject data) {
        return new AssetClaimFeesOperation(
                OperationType.ASSET_CLAIM_FEES, data, accountRepository
        );
    }

    public UpdateUserIssuedAssetOperation updateUserIssuedAsset(JSONObject data) {
        return new UpdateUserIssuedAssetOperation(
                OperationType.UPDATE_USER_ISSUED_ASSET, data, accountRepository
        );
    }

    public UpdateMonitoredAssetOperation updateMonitoredAsset(JSONObject data) {
        return new UpdateMonitoredAssetOperation(
                OperationType.UPDATE_MONITORED_ASSET, data, accountRepository
        );
    }

    public ReadyToPublish2Operation readyToPublish2(JSONObject data) {
        return new ReadyToPublish2Operation(
                OperationType.READY_TO_PUBLISH2, data, accountRepository
        );
    }

    public DisallowAutomaticRenewalOfSubscriptionOperation disallowAutomaticRenewalOfSubscription(JSONObject data) {
        return new DisallowAutomaticRenewalOfSubscriptionOperation(
                OperationType.DISALLOW_AUTOMATIC_RENEWAL_OF_SUBSCRIPTION, data, accountRepository
        );
    }

    public ReturnEscrowSubmissionOperation returnEscrowSubmission(JSONObject data) {
        return new ReturnEscrowSubmissionOperation(
                OperationType.RETURN_ESCROW_SUBMISSION, data, accountRepository
        );
    }

    public ReturnEscrowBuyingOperation returnEscrowBuying(JSONObject data) {
        return new ReturnEscrowBuyingOperation(
                OperationType.RETURN_ESCROW_BUYING, data, accountRepository
        );
    }

    public PaySeederOperation paySeeder(JSONObject data) {
        return new PaySeederOperation(
                OperationType.PAY_SEEDER, data, accountRepository
        );
    }

    public FinishBuyingOperation finishBuying(JSONObject data) {
        return new FinishBuyingOperation(
                OperationType.FINISH_BUYING, data, accountRepository
        );
    }

    public RenewalOfSubscriptionOperation renewalOfSubscription(JSONObject data) {
        return new RenewalOfSubscriptionOperation(
                OperationType.RENEWAL_OF_SUBSCRIPTION, data, accountRepository
        );
    }
}
