package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This operation is used to withdraw from an account which has authorized
 * such a withdrawal. It may be executed at most once per withdrawal period
 * for the given permission. On execution, amount_to_withdraw is transferred
 * from withdraw_from_account to withdraw_to_account, assuming
 * amount_to_withdraw is within the withdrawal limit. The withdrawal permission
 * will be updated to note that the withdrawal for the current period has
 * occurred, and further withdrawals will not be permitted until the next
 * withdrawal period, assuming the permission has not expired. This operation
 * may be executed at any time within the current withdrawal period.
 */
public class WithdrawPermissionClaimOperation extends AbstractOperation {
    WithdrawPermissionClaimOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("withdraw_to_account");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("withdraw_from_account");
    }
}
