package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Withdraw from a vesting balance.
 *
 * Withdrawal from a not-completely-mature vesting balance
 * will result in paying fees.
 */
public class VestingBalanceWithdrawOperation extends AbstractOperation {
    VestingBalanceWithdrawOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("owner");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) {
        return null;
    }
}
