package com.sophiatx.explorer.blockchain.operation;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.json.JSONException;
import org.json.JSONObject;

public class FinishBuyingOperation extends AbstractOperation {
    FinishBuyingOperation(
            OperationType type,
            JSONObject data,
            AccountRepository accountRepository
    ) {
        super(type, data, accountRepository);
    }

    @Override
    protected String parseSenderAccountId(JSONObject data) throws JSONException {
        return data.getString("author");
    }

    @Override
    protected String parseTargetAccountId(JSONObject data) throws JSONException {
        return data.getString("consumer");
    }
}
