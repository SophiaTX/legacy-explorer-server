package com.sophiatx.explorer.blockchain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sophiatx.explorer.configuration.ApplicationConfiguration;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Component
public class DaemonConnector {
    private final ApplicationConfiguration appConfig;

    @Autowired
    public DaemonConnector(ApplicationConfiguration appConfig) {
        this.appConfig = appConfig;
    }

    public Optional<DaemonResponse> execute(DaemonRequest dRequest) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost req = buildRequest(dRequest);
            CloseableHttpResponse res = client.execute(req);
            String body = IOUtils.toString(res.getEntity().getContent(), "UTF-8");

            return Optional.of(
                    new DaemonResponse(new JSONObject(body))
            );
        } catch (IOException | JSONException e) {
            return Optional.empty();
        }
    }

    private HttpPost buildRequest(DaemonRequest dRequest) {
        String json = buildJson(dRequest.getAction(), dRequest.getArguments());
        try {
            String nodeAddress = appConfig.getNode().getFullAddress(true);
            HttpPost request = new HttpPost(nodeAddress);

            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            request.setEntity(new StringEntity(json));

            return request;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    private static String buildJson(String action, List<Object> arguments) {
        ObjectMapper mapper = new ObjectMapper();

        ArrayNode args = mapper.createArrayNode();
        for (Object object : arguments)
            addObjectToNode(object, args, mapper);

        ArrayNode params = mapper.createArrayNode();
        params.add(0);
        params.add(action);
        params.add(args);

        ObjectNode request = mapper.createObjectNode();
        request.put("id", 1);
        request.put("method", "call");
        request.set("params", params);

        return request.toString();
    }

    /**
     * Used to recursively build arguments ArrayNode
     *
     * @param object The object we want to add to the arguments
     * @param node   Node we want to add it to
     * @param mapper The mapper we will use to create sub-nodes.
     */
    private static void addObjectToNode(
            Object object,
            ArrayNode node,
            ObjectMapper mapper
    ) {
        if (object instanceof List) {
            ArrayNode list = mapper.createArrayNode();
            for (Object o : (List) object)
                addObjectToNode(o, list, mapper);
            node.add(list);
        } else if (object instanceof String)
            node.add((String) object);
        else if (object instanceof Integer)
            node.add((Integer) object);
        else if (object instanceof Float)
            node.add((Float) object);
        else if (object instanceof Long)
            node.add((Long) object);
    }
}
