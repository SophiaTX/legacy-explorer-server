package com.sophiatx.explorer.blockchain.model;

import com.sophiatx.explorer.helper.ISO8601;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;

public class DynamicGlobalProperties {
    private Long headBlockNumber;
    private String headBlockId;
    private LocalDateTime time;
    private String currentMiner;
    private LocalDateTime nextMaintenanceTime;
    private LocalDateTime lastBudgetTime;
    private Long unspentFeeBudget;
    private Long minedRewards;
    private Long minerBudgetFromFees;
    private Long minerBudgetFromRewards;
    private Integer accountsRegisteredThisInterval;
    private Integer recentlyMissedCount;
    private Integer currentAslot;
    private String recentSlotsFilled;
    private Integer dynamicFlags;
    private Long lastIrreversibleBlockNum;

    public DynamicGlobalProperties(
            Long headBlockNumber,
            String headBlockId,
            LocalDateTime time,
            String currentMiner,
            LocalDateTime nextMaintenanceTime,
            LocalDateTime lastBudgetTime,
            Long unspentFeeBudget,
            Long minedRewards,
            Long minerBudgetFromFees,
            Long minerBudgetFromRewards,
            Integer accountsRegisteredThisInterval,
            Integer recentlyMissedCount,
            Integer currentAslot,
            String recentSlotsFilled,
            Integer dynamicFlags,
            Long lastIrreversibleBlockNum
    ) {
        this.headBlockNumber = headBlockNumber;
        this.headBlockId = headBlockId;
        this.time = time;
        this.currentMiner = currentMiner;
        this.nextMaintenanceTime = nextMaintenanceTime;
        this.lastBudgetTime = lastBudgetTime;
        this.unspentFeeBudget = unspentFeeBudget;
        this.minedRewards = minedRewards;
        this.minerBudgetFromFees = minerBudgetFromFees;
        this.minerBudgetFromRewards = minerBudgetFromRewards;
        this.accountsRegisteredThisInterval = accountsRegisteredThisInterval;
        this.recentlyMissedCount = recentlyMissedCount;
        this.currentAslot = currentAslot;
        this.recentSlotsFilled = recentSlotsFilled;
        this.dynamicFlags = dynamicFlags;
        this.lastIrreversibleBlockNum = lastIrreversibleBlockNum;
    }

    public Long getHeadBlockNumber() {
        return headBlockNumber;
    }

    public String getHeadBlockId() {
        return headBlockId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public String getCurrentMiner() {
        return currentMiner;
    }

    public LocalDateTime getNextMaintenanceTime() {
        return nextMaintenanceTime;
    }

    public LocalDateTime getLastBudgetTime() {
        return lastBudgetTime;
    }

    public Long getUnspentFeeBudget() {
        return unspentFeeBudget;
    }

    public Long getMinedRewards() {
        return minedRewards;
    }

    public Long getMinerBudgetFromFees() {
        return minerBudgetFromFees;
    }

    public Long getMinerBudgetFromRewards() {
        return minerBudgetFromRewards;
    }

    public Integer getAccountsRegisteredThisInterval() {
        return accountsRegisteredThisInterval;
    }

    public Integer getRecentlyMissedCount() {
        return recentlyMissedCount;
    }

    public Integer getCurrentAslot() {
        return currentAslot;
    }

    public String getRecentSlotsFilled() {
        return recentSlotsFilled;
    }

    public Integer getDynamicFlags() {
        return dynamicFlags;
    }

    public Long getLastIrreversibleBlockNum() {
        return lastIrreversibleBlockNum;
    }

    public JSONObject toJsonObject() {
        JSONObject obj = new JSONObject();
        obj.put("headBlockNumber", headBlockNumber);
        obj.put("headBlockId", headBlockId);
        obj.put("time", ISO8601.toString(time));
        obj.put("currentMiner", currentMiner);
        obj.put("nextMaintenanceTime", ISO8601.toString(nextMaintenanceTime));
        obj.put("lastBudgetTime", ISO8601.toString(lastBudgetTime));
        obj.put("unspentFeeBudget", unspentFeeBudget);
        obj.put("minedRewards", minedRewards);
        obj.put("minerBudgetFromFees", minerBudgetFromFees);
        obj.put("minerBudgetFromRewards", minerBudgetFromRewards);
        obj.put("accountsRegisteredThisInterval", accountsRegisteredThisInterval);
        obj.put("recentlyMissedCount", recentlyMissedCount);
        obj.put("currentAslot", currentAslot);
        obj.put("recentSlotsFilled", recentSlotsFilled);
        obj.put("dynamicFlags", dynamicFlags);
        obj.put("lastIrreversibleBlockNum", lastIrreversibleBlockNum);
        return obj;
    }

    public static DynamicGlobalProperties fromJsonObject(JSONObject data)
            throws JSONException {
        return new DynamicGlobalProperties(
                data.getLong("headBlockNumber"),
                data.getString("headBlockId"),
                ISO8601.toDate(data.getString("time")),
                data.getString("currentMiner"),
                ISO8601.toDate(data.getString("nextMaintenanceTime")),
                ISO8601.toDate(data.getString("lastBudgetTime")),
                data.getLong("unspentFeeBudget"),
                data.getLong("minedRewards"),
                data.getLong("minerBudgetFromFees"),
                data.getLong("minerBudgetFromRewards"),
                data.getInt("accountsRegisteredThisInterval"),
                data.getInt("recentlyMissedCount"),
                data.getInt("currentAslot"),
                data.getString("recentSlotsFilled"),
                data.getInt("dynamicFlags"),
                data.getLong("lastIrreversibleBlockNum")
        );
    }
}
