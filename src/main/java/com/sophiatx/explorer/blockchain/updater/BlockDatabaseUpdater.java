package com.sophiatx.explorer.blockchain.updater;

import com.sophiatx.explorer.blockchain.model.DynamicGlobalProperties;
import com.sophiatx.explorer.blockchain.request.ResourceRequestFactory;
import com.sophiatx.explorer.database.model.Block;
import com.sophiatx.explorer.database.model.JsonEntity;
import com.sophiatx.explorer.database.repository.BlockRepository;
import com.sophiatx.explorer.database.repository.JsonEntityRepository;
import com.sophiatx.explorer.database.repository.MinerRepository;
import com.sophiatx.explorer.event.NewBlockReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class BlockDatabaseUpdater {
    private final BlockRepository blockRepository;
    private final MinerRepository minerRepository;
    private final JsonEntityRepository jsonEntityRepository;
    private final ApplicationEventPublisher publisher;
    private final ResourceRequestFactory resourceRequest;
    private static final Logger logger
            = LoggerFactory.getLogger(BlockDatabaseUpdater.class);

    @Autowired
    public BlockDatabaseUpdater(
            BlockRepository blockRepository,
            MinerRepository minerRepository,
            JsonEntityRepository jsonEntityRepository,
            ApplicationEventPublisher publisher,
            ResourceRequestFactory resourceRequestFactory) {
        this.blockRepository = blockRepository;
        this.minerRepository = minerRepository;
        this.jsonEntityRepository = jsonEntityRepository;
        this.publisher = publisher;
        this.resourceRequest = resourceRequestFactory;
    }

    public void fetchMissingBlocks() {
        logger.info("Fetching missing blocks.");

        long s = getLastStoredBlockHeight();
        long l = getLatestBlockHeight();

        if (l > s) {
            fetchBlockRange(s + 1, l);
        } else {
            logger.info("There are no missing blocks. Nothing to do.");
        }
    }

    /**
     * Fetch blocks
     *
     * @param from Block height from ( inclusive )
     * @param to   Block height to ( inclusive )
     */
    private void fetchBlockRange(Long from, Long to) {
        logger.info("Fetching " + (to - from + 1) + " blocks.");

        for (long i = from; i <= to; i++)
            fetchBlock(i);

        blockRepository.flush();
        minerRepository.flush(); // miners' totalMined property

        logger.info("Done fetching blocks.");
    }

    private long getLastStoredBlockHeight() {
        return blockRepository.findFirst1ByOrderByHeightDesc()
                .map(Block::getHeight)
                .orElse(0L);
    }

    private long getLatestBlockHeight() {
        Optional<DynamicGlobalProperties> dgp =
                resourceRequest.dynamicGlobalProperties().getResult();

        if (dgp.isPresent()) {
            this.jsonEntityRepository.saveAndFlush(new JsonEntity(
                    "dynamicGlobalProperties",
                    dgp.get().toJsonObject()
            ));
            return dgp.get().getHeadBlockNumber();
        } else {
            return 0L;
        }
    }

    private void fetchBlock(Long height) {
        Optional<Block> b = resourceRequest.block(height).getResult();

        if (!b.isPresent()) {
            logger.error("Failed to fetch block " + height);
        } else {
            Block block = b.get();
            blockRepository.save(block);
            publisher.publishEvent(new NewBlockReceivedEvent(block));
        }
    }
}
