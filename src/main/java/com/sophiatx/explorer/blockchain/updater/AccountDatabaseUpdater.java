package com.sophiatx.explorer.blockchain.updater;

import com.sophiatx.explorer.blockchain.request.ResourceRequestFactory;
import com.sophiatx.explorer.database.model.Account;
import com.sophiatx.explorer.database.repository.AccountRepository;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AccountDatabaseUpdater {
    private final AccountRepository accountRepository;
    private final ResourceRequestFactory resourceRequestFactory;
    private static final Logger logger
            = LoggerFactory.getLogger(AccountDatabaseUpdater.class);

    @Autowired
    public AccountDatabaseUpdater(
            AccountRepository accountRepository,
            ResourceRequestFactory resourceRequestFactory) {
        this.accountRepository = accountRepository;
        this.resourceRequestFactory = resourceRequestFactory;
    }

    public void updateBalances() {
        logger.info("Updating account balances.");
        accountRepository.findAll().forEach(account -> resourceRequestFactory
                .accountBalances("1.2." + account.getId())
                .getResult()
                .ifPresent(accountBalances -> {
                    account.setBalances(accountBalances);
                    accountRepository.save(account);
                })
        );
        accountRepository.flush();
        logger.info("Done updating account balances.");
    }

    public void fetchMissingAccounts() {
        logger.info("Fetching missing accounts.");

        long storedId = getLastStoredAccountId();
        long latestId = getAccountCount() - 1;

        if (latestId > storedId) {
            fetchAccountRange(storedId + 1, latestId);
        } else {
            logger.info("There are no missing accounts. Nothing to do.");
        }
    }

    /**
     * Fetch accounts
     *
     * @param from Account ID from ( inclusive )
     * @param to   Account ID to ( inclusive )
     */
    private void fetchAccountRange(Long from, Long to) {
        List<String> ids = new ArrayList<>();

        for (long i = from; i <= to; i++)
            ids.add("1.2." + i);

        logger.info("Fetching " + (to - from + 1) + " accounts.");

        for (List<String> stack : ListUtils.partition(ids, 1000))
            resourceRequestFactory.accounts(stack).forceFetch();

        logger.info("Done fetching accounts.");
    }

    private Integer getLastStoredAccountId() {
        return accountRepository.findFirst1ByOrderByIdDesc()
                .map(Account::getId)
                .orElse(-1);
    }

    private Integer getAccountCount() {
        return resourceRequestFactory.accountCount().getResult().orElse(0);
    }
}
