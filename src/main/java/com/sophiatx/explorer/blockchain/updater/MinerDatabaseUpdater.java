package com.sophiatx.explorer.blockchain.updater;

import com.sophiatx.explorer.blockchain.request.ResourceRequestFactory;
import com.sophiatx.explorer.database.model.Miner;
import com.sophiatx.explorer.database.repository.MinerRepository;
import org.apache.commons.collections4.ListUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MinerDatabaseUpdater {
    private final MinerRepository minerRepository;
    private final ResourceRequestFactory resourceRequestFactory;
    private static final Logger logger
            = LoggerFactory.getLogger(MinerDatabaseUpdater.class);

    public static final int UPDATE_MINER = 1;
    public static final int OVERRIDE_MINER = 2;

    @Autowired
    public MinerDatabaseUpdater(
            MinerRepository minerRepository,
            ResourceRequestFactory resourceRequestFactory) {
        this.minerRepository = minerRepository;
        this.resourceRequestFactory = resourceRequestFactory;
    }

    public void updateExistingMiners() {
        logger.info("Updating already existing miners.");

        Integer lastStoredMinerId = getLastStoredMinerId();

        if (lastStoredMinerId > 1) {
            fetchAndSaveMinerRange(1, getLastStoredMinerId(), UPDATE_MINER);
        } else {
            logger.info("There are no already existing miners. Nothing to do.");
        }
    }

    public void fetchMissingMiners() {
        logger.info("Fetching missing miners.");

        Integer storedMinerId = getLastStoredMinerId();
        Integer lastMinerId = getMinerCount(); // miners start at 1, not 0

        if (lastMinerId > storedMinerId) {
            fetchAndSaveMinerRange(storedMinerId + 1, lastMinerId, OVERRIDE_MINER);
        } else {
            logger.info("There are no missing miners. Nothing to do.");
        }
    }

    /**
     * Fetch accounts
     *
     * @param from Miner ID from ( inclusive )
     * @param to   Miner ID to ( inclusive )
     */
    private void fetchAndSaveMinerRange(Integer from, Integer to, int method) {
        List<String> ids = new ArrayList<>();

        for (long i = from; i <= to; i++)
            ids.add("1.4." + i);

        logger.info("Fetching " + (to - from + 1) + " miners.");

        for (List<String> stack : ListUtils.partition(ids, 1000)) {
            resourceRequestFactory.miners(stack).getResult().ifPresent(
                    miners -> {
                        if (method == UPDATE_MINER) {
                            miners.forEach(this::updateMinerProps);
                        } else {
                            miners.forEach(minerRepository::save);
                        }
                    }
            );
        }
        minerRepository.flush();

        logger.info("Done fetching miners.");
    }

    private void updateMinerProps(Miner miner) {
        minerRepository.findById(miner.getId()).ifPresent(oldMiner -> {
            oldMiner.setTotalMissed(miner.getTotalMissed())
                    .setTotalVotes(miner.getTotalVotes());
            minerRepository.save(oldMiner);
        });
    }

    private Integer getLastStoredMinerId() {
        return minerRepository.findFirst1ByOrderByIdDesc()
                .map(Miner::getId)
                .orElse(0);
    }

    private Integer getMinerCount() {
        return resourceRequestFactory.minerCount().getResult().orElse(0);
    }
}
