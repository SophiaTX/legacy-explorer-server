package com.sophiatx.explorer.blockchain;

import java.util.ArrayList;
import java.util.List;

public class DaemonRequest {
    private String action;
    private List<Object> arguments;

    public DaemonRequest(String action) {
        this.action = action;
        this.arguments = new ArrayList<>();
    }

    public DaemonRequest(String action, List<Object> arguments) {
        this.action = action;
        this.arguments = arguments;
    }

    public String getAction() {
        return action;
    }

    public DaemonRequest setAction(String action) {
        this.action = action;
        return this;
    }

    public List<Object> getArguments() {
        return arguments;
    }

    public DaemonRequest setArguments(List<Object> arguments) {
        this.arguments = arguments;
        return this;
    }

    public DaemonRequest withArgument(Object argument) {
        this.arguments.add(argument);
        return this;
    }
}
