package com.sophiatx.explorer.blockchain;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DaemonResponse {
    private static String RESULT_KEY = "result";
    private JSONObject data;

    public DaemonResponse(JSONObject data) {
        this.data = data;
    }

    public JSONObject getData() {
        return data;
    }

    public JSONObject getAsObject() throws JSONException {
        return data.getJSONObject(RESULT_KEY);
    }

    public JSONArray getAsArray() throws JSONException {
        return data.getJSONArray(RESULT_KEY);
    }

    public Integer getAsInteger() throws JSONException {
        return data.getInt(RESULT_KEY);
    }

    public Long getAsLong() throws JSONException {
        return data.getLong(RESULT_KEY);
    }

    public Float getAsFloat() throws JSONException {
        return data.getFloat(RESULT_KEY);
    }

    public Double getAsDouble() throws JSONException {
        return data.getDouble(RESULT_KEY);
    }

    public <E extends Enum<E>> E getAsEnum(Class<E> clazz) throws JSONException {
        return data.getEnum(clazz, RESULT_KEY);
    }

    public String getAsString() throws JSONException {
        return data.getString(RESULT_KEY);
    }

    public Boolean getAsBoolean() throws JSONException {
        return data.getBoolean(RESULT_KEY);
    }
}
