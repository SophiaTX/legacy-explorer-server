package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.DaemonRequest;
import com.sophiatx.explorer.blockchain.operation.AbstractOperationFactory;
import com.sophiatx.explorer.database.id.TransactionId;
import com.sophiatx.explorer.database.model.Block;
import com.sophiatx.explorer.database.model.Miner;
import com.sophiatx.explorer.database.model.Operation;
import com.sophiatx.explorer.database.model.Transaction;
import com.sophiatx.explorer.database.repository.MinerRepository;
import com.sophiatx.explorer.helper.ISO8601;
import com.sophiatx.explorer.helper.IdConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BlockRequest extends AbstractResourceRequest<Block> {
    private final Long height;
    private final MinerRepository minerRepository;
    private final AbstractOperationFactory abstractOperationFactory;

    BlockRequest(
            Long height,
            MinerRepository minerRepository,
            DaemonConnector dConnector,
            AbstractOperationFactory abstractOperationFactory
    ) {
        super(dConnector);
        this.height = height;
        this.minerRepository = minerRepository;
        this.abstractOperationFactory = abstractOperationFactory;
    }

    @Override
    protected Optional<Block> execute() {
        return dConnector.execute(
                new DaemonRequest("get_block").withArgument(height)
        ).map(daemonResponse -> {
            try {
                return processBlock(daemonResponse.getAsObject(), height);
            } catch (JSONException e) {
                logger.error("Unable to parse block.");
                return null;
            } catch (Exception e) {
                logger.error("Failed to process block " + height + ".");
                e.printStackTrace();
                return null;
            }
        });
    }

    private Block processBlock(JSONObject obj, Long height)
            throws JSONException, IllegalStateException {
        Miner miner = minerRepository.findById(
                IdConverter.blockchainToInt(obj.getString("miner"))
        ).orElseThrow(() -> new IllegalStateException(
                "Miners must be fetched first before reading the blocks. " +
                        "Unable to find miner: " + obj.getString("miner")
        ));

        miner.incrementMinedBlocksCount();
        minerRepository.save(miner);

        Block block = new Block(
                height,
                ISO8601.toDate(obj.getString("timestamp")),
                miner,
                obj.getString("miner_signature")
        );

        processTransactions(block, obj.getJSONArray("transactions"));

        int txCnt = block.getTransactions().size();
        int opCnt = block.getTransactions().stream()
                .mapToInt(t -> t.getOperations().size()).sum();

        block.setTransactionCount(txCnt);
        block.setOperationCount(opCnt);

        return block;
    }

    private void processTransactions(Block block, JSONArray ary) throws JSONException {
        for (int orderInBlock = 0; orderInBlock < ary.length(); orderInBlock++) {
            JSONObject t = (JSONObject) ary.get(orderInBlock);

            Transaction transaction = new Transaction(
                    new TransactionId().setOrderInBlock(orderInBlock),
                    t.getLong("ref_block_num"),
                    t.getLong("ref_block_prefix"),
                    processSignatures(t.getJSONArray("signatures"))
            );
            block.addTransaction(transaction);

            processOperations(transaction, t.getJSONArray("operations"));
        }
    }

    private void processOperations(
            Transaction t,
            JSONArray ary
    ) throws JSONException {
        for (int i = 0; i < ary.length(); i++) {
            JSONArray op = (JSONArray) ary.get(i);
            final int txo = i; // lambda requires ( effectively ) final vars

            this.abstractOperationFactory.fromTypeAndData(
                    op.getInt(0),
                    op.getJSONObject(1)
            ).ifPresent(o -> {
                Operation operation = o.getAsModel();
                operation.getId().setOrderInTransaction(txo);

                t.addOperation(operation);
            });
        }
    }

    private List<String> processSignatures(JSONArray ary) throws JSONException {
        List<String> signatures = new ArrayList<>();

        for (int i = 0; i < ary.length(); i++)
            signatures.add(ary.getString(i));

        return signatures;
    }
}
