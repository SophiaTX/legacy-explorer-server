package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

abstract class AbstractResourceRequest<T> {
    DaemonConnector dConnector;

    protected final Logger logger = LoggerFactory.getLogger(AbstractResourceRequest.class);
    private boolean executed = false;

    // we are just mapping the original Optional response from Daemon
    // so there is no reason to .get() and then Optional.ofNull() it.
    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<T> result;

    AbstractResourceRequest(DaemonConnector dConnector) {
        this.dConnector = dConnector;
    }

    public Optional<T> getResult() throws IllegalStateException {
        if (!this.executed) {
            this.result = this.execute();
            this.executed = true;
        }

        return this.result;
    }

    public void forceFetch() {
        this.result = this.execute();
        this.executed = true;
    }

    protected abstract Optional<T> execute();
}
