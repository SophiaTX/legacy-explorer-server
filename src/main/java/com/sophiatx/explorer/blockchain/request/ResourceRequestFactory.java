package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.operation.AbstractOperationFactory;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.database.repository.MinerRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ResourceRequestFactory {
    private final DaemonConnector daemonConnector;
    private final AccountRepository accountRepository;
    private final MinerRepository minerRepository;
    private final AbstractOperationFactory abstractOperationFactory;

    ResourceRequestFactory(
            DaemonConnector daemonConnector,
            AccountRepository accountRepository,
            MinerRepository minerRepository,
            AbstractOperationFactory abstractOperationFactory
    ) {
        this.daemonConnector = daemonConnector;
        this.accountRepository = accountRepository;
        this.minerRepository = minerRepository;
        this.abstractOperationFactory = abstractOperationFactory;
    }

    public AccountBalancesRequest accountBalances(String accountId) {
        return new AccountBalancesRequest(accountId, daemonConnector);
    }

    public AccountCountRequest accountCount() {
        return new AccountCountRequest(daemonConnector);
    }

    public AccountsRequest accounts(List<String> ids) {
        return new AccountsRequest(ids, accountRepository, daemonConnector);
    }

    public BlockRequest block(Long height) {
        return new BlockRequest(
                height,
                minerRepository,
                daemonConnector,
                abstractOperationFactory
        );
    }

    public DynamicGlobalPropertiesRequest dynamicGlobalProperties() {
        return new DynamicGlobalPropertiesRequest(daemonConnector);
    }

    public MinerCountRequest minerCount() {
        return new MinerCountRequest(daemonConnector);
    }

    public MinersRequest miners(List<String> ids) {
        return new MinersRequest(ids, accountRepository, daemonConnector);
    }
}
