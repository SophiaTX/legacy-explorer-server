package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.DaemonRequest;
import com.sophiatx.explorer.database.model.Account;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.helper.IdConverter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountsRequest extends AbstractResourceRequest<List<Account>> {
    private List<String> ids;

    private AccountRepository accountRepository;

    AccountsRequest(
            List<String> ids,
            AccountRepository accountRepository,
            DaemonConnector dConnector
    ) {
        super(dConnector);
        this.ids = ids;
        this.accountRepository = accountRepository;
    }

    @Override
    protected Optional<List<Account>> execute() {
        return dConnector.execute(
                new DaemonRequest("get_accounts").withArgument(this.ids)
        ).map(daemonResponse -> {
            List<Account> accounts = new ArrayList<>();

            try {
                for (Object obj : daemonResponse.getAsArray()) {
                    JSONObject a = (JSONObject) obj;
                    Account account = processAccount(a);
                    new AccountBalancesRequest("1.2." + account.getId(), dConnector)
                            .getResult()
                            .ifPresent(accountBalances -> {
                                account.setBalances(accountBalances);
                                accountRepository.save(account);
                            });
                    accounts.add(account);
                }

                return accounts;
            } catch (JSONException e) {
                logger.error("Unable to parse accounts.");
                return null;
            }
        });
    }

    private Account processAccount(JSONObject obj) throws JSONException {
        Account registrar = accountRepository.findById(
                IdConverter.blockchainToInt(obj.getString("registrar"))
        ).orElse(null);

        Account account = new Account(
                IdConverter.blockchainToInt(obj.getString("id")),
                obj.getString("name"),
                registrar
        );

        // we need to flush the account every time as the registrar is being
        // queried on each iteration and the next account may be the one that
        // "belongs" to this one, which would result in a null field in the DB.
        accountRepository.saveAndFlush(account);

        return account;
    }
}
