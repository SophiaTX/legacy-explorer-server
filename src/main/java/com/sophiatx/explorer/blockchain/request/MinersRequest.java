package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.DaemonRequest;
import com.sophiatx.explorer.database.model.Account;
import com.sophiatx.explorer.database.model.Miner;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.helper.IdConverter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MinersRequest extends AbstractResourceRequest<List<Miner>> {
    private List<String> ids;
    private AccountRepository accountRepository;

    MinersRequest(
            List<String> ids,
            AccountRepository accountRepository,
            DaemonConnector dConnector
    ) {
        super(dConnector);
        this.ids = ids;
        this.accountRepository = accountRepository;
    }

    @Override
    protected Optional<List<Miner>> execute() {
        return this.dConnector.execute(
                new DaemonRequest("get_miners").withArgument(ids)
        ).map(daemonResponse -> {
            List<Miner> miners = new ArrayList<>();
            try {
                for (Object obj : daemonResponse.getAsArray()) {
                    Miner miner = processMiner((JSONObject) obj);
                    miners.add(miner);
                }
                return miners;
            } catch (JSONException e) {
                logger.error("Unable to parse miners.");
                return null;
            }
        });
    }

    private Miner processMiner(JSONObject obj) throws JSONException {
        Account account = accountRepository.findById(
                IdConverter.blockchainToInt(obj.getString("miner_account"))
        ).orElse(null);

        return new Miner(
                IdConverter.blockchainToInt(obj.getString("id")),
                obj.getLong("total_votes"),
                obj.getLong("total_missed"),
                account
        );
    }
}
