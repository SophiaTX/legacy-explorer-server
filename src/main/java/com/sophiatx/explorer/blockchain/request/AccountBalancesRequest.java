package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.DaemonRequest;
import com.sophiatx.explorer.helper.IdConverter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class AccountBalancesRequest extends AbstractResourceRequest<Map<Integer, Long>> {
    private String accountId;

    AccountBalancesRequest(String accountId, DaemonConnector dConnector) {
        super(dConnector);
        this.accountId = accountId;
    }

    @Override
    protected Optional<Map<Integer, Long>> execute() {
        return this.dConnector.execute(
                new DaemonRequest("get_account_balances")
                        .withArgument(accountId)
                        .withArgument(new ArrayList<String>())
        ).map(daemonResponse -> {
            Map<Integer, Long> balances = new HashMap<>();

            try {
                for (Object b : daemonResponse.getAsArray()) {
                    JSONObject balance = (JSONObject) b;

                    final Integer asset = IdConverter.blockchainToInt(
                            balance.getString("asset_id")
                    );

                    balances.put(asset, balance.getLong("amount"));
                }
            } catch (JSONException e) {
                logger.error("Unable to parse account balances.");
                return null;
            }

            return balances;
        });
    }
}
