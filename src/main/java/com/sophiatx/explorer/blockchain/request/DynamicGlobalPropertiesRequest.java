package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.DaemonRequest;
import com.sophiatx.explorer.blockchain.model.DynamicGlobalProperties;
import com.sophiatx.explorer.helper.ISO8601;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Optional;

public class DynamicGlobalPropertiesRequest extends AbstractResourceRequest<DynamicGlobalProperties> {
    DynamicGlobalPropertiesRequest(DaemonConnector dConnector) {
        super(dConnector);
    }

    @Override
    protected Optional<DynamicGlobalProperties> execute() {
        return this.dConnector.execute(
                new DaemonRequest("get_dynamic_global_properties")
        ).map(daemonResponse -> {
            try {
                JSONObject obj = daemonResponse.getAsObject();

                return new DynamicGlobalProperties(
                        obj.getLong("head_block_number"),
                        obj.getString("head_block_id"),
                        ISO8601.toDate(obj.getString("time")),
                        obj.getString("current_miner"),
                        ISO8601.toDate(obj.getString("next_maintenance_time")),
                        ISO8601.toDate(obj.getString("last_budget_time")),
                        obj.getLong("unspent_fee_budget"),
                        obj.getLong("mined_rewards"),
                        obj.getLong("miner_budget_from_fees"),
                        obj.getLong("miner_budget_from_rewards"),
                        obj.getInt("accounts_registered_this_interval"),
                        obj.getInt("recently_missed_count"),
                        obj.getInt("current_aslot"),
                        obj.getString("recent_slots_filled"),
                        obj.getInt("dynamic_flags"),
                        obj.getLong("last_irreversible_block_num")
                );
            } catch (JSONException e) {
                this.logger.error("Unable to parse dynamic global properties.");
                return null;
            }
        });
    }
}
