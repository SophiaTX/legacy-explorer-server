package com.sophiatx.explorer.blockchain.request;

import com.sophiatx.explorer.blockchain.DaemonConnector;
import com.sophiatx.explorer.blockchain.DaemonRequest;
import org.json.JSONException;

import java.util.Optional;

public class MinerCountRequest extends AbstractResourceRequest<Integer> {
    MinerCountRequest(DaemonConnector dConnector) {
        super(dConnector);
    }

    @Override
    protected Optional<Integer> execute() {
        return dConnector.execute(
                new DaemonRequest("get_miner_count")
        ).map(daemonResponse -> {
            try {
                return daemonResponse.getAsInteger();
            } catch (JSONException e) {
                this.logger.error("Unable to parse miner count.");
                return null;
            }
        });
    }
}
