package com.sophiatx.explorer.api.controller.params;

public class IntRange extends RangeObject<Integer> {
    public IntRange(Integer from, Integer to) {
        super(from, to);
    }
}
