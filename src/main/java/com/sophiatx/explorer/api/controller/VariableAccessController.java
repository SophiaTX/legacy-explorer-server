package com.sophiatx.explorer.api.controller;

import com.sophiatx.explorer.api.exception.ResourceNotFoundException;
import com.sophiatx.explorer.api.resource.DynamicGlobalPropertiesResource;
import com.sophiatx.explorer.blockchain.model.DynamicGlobalProperties;
import com.sophiatx.explorer.database.repository.JsonEntityRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Dynamic variables", description = "Access to dynamic variables / objects")
@RestController
@RequestMapping(value = "/var", produces = {MediaType.APPLICATION_JSON_VALUE})
public class VariableAccessController {
    private JsonEntityRepository jsonEntityRepository;

    public VariableAccessController(JsonEntityRepository jsonEntityRepository) {
        this.jsonEntityRepository = jsonEntityRepository;
    }

    @ApiOperation("Get dynamic global properties")
    @GetMapping("dynamicGlobalProperties")
    public DynamicGlobalPropertiesResource getDynamicGlobalProperties() {
        return this.jsonEntityRepository
                .findByKey("dynamicGlobalProperties")
                .map(jsonEntity -> new DynamicGlobalPropertiesResource(
                        DynamicGlobalProperties.fromJsonObject(jsonEntity.getData())
                )).orElseThrow(() ->
                        new ResourceNotFoundException("dynamicGlobalProperties")
                );
    }
}
