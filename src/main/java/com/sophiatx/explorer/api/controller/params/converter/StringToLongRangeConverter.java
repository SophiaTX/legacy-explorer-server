package com.sophiatx.explorer.api.controller.params.converter;

import com.sophiatx.explorer.api.controller.params.LongRange;
import com.sophiatx.explorer.api.exception.InvalidParameterFormatException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToLongRangeConverter implements Converter<String, LongRange> {
    private static final String FORMAT = "^[0-9]+(?:-[0-9]+)?$";

    @Override
    public LongRange convert(String source)
            throws InvalidParameterFormatException,
                   NumberFormatException {
        if (!source.matches(FORMAT)) {
            throw new InvalidParameterFormatException(source, FORMAT);
        }

        String[] parts = source.split("-");

        return new LongRange(
                Long.parseLong(parts[0]),
                Long.parseLong(parts.length == 1 ? parts[0] : parts[1])
        );
    }
}
