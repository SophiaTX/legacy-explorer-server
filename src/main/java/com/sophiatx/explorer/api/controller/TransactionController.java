package com.sophiatx.explorer.api.controller;

import com.sophiatx.explorer.api.exception.ResourceNotFoundException;
import com.sophiatx.explorer.api.exception.TooManyObjectsRequestedException;
import com.sophiatx.explorer.api.resource.CountResource;
import com.sophiatx.explorer.api.resource.TransactionResource;
import com.sophiatx.explorer.database.OffsetBasedPageRequest;
import com.sophiatx.explorer.database.id.TransactionId;
import com.sophiatx.explorer.database.repository.BlockRepository;
import com.sophiatx.explorer.database.repository.TransactionRepository;
import com.sophiatx.explorer.lang.ApiDoc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Api(tags = "Transactions", description = "Access to transactions")
@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
public class TransactionController {
    private static final int MAX_TRANSACTIONS_PER_REQUEST = 1000;

    private final BlockRepository blockRepository;
    private final TransactionRepository transactionRepository;

    @Autowired
    public TransactionController(
            TransactionRepository transactionRepository,
            BlockRepository blockRepository
    ) {
        this.transactionRepository = transactionRepository;
        this.blockRepository = blockRepository;
    }

    @ApiOperation("List transactions")
    @GetMapping("/transactions")
    public Collection<TransactionResource> getTransactions(
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(
                    value = "offset",
                    defaultValue = "0"
            ) final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(
                    value = "limit",
                    defaultValue = "10"
            ) final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(
                    value = "sortDir",
                    defaultValue = "ASC"
            ) final Sort.Direction sortDir
    ) {
        if (limit > MAX_TRANSACTIONS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_TRANSACTIONS_PER_REQUEST);

        final OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(
                offset, limit, new Sort(sortDir, "id")
        );

        final List<TransactionResource> transactions = new ArrayList<>();
        this.transactionRepository.findAll(pageRequest)
                .forEach(t -> transactions.add(new TransactionResource(t)));

        return transactions;
    }

    @ApiOperation("Get number of transactions")
    @GetMapping("/transactions/count")
    public CountResource getTransactionCount() {
        return new CountResource(this.transactionRepository.count());
    }

    @ApiOperation("Get transaction by composite ID")
    @GetMapping("/transactions/B{height:[0-9]+}T{tx:[0-9+]}")
    public TransactionResource getTransactionByChainId(
            @ApiParam(value = "Block height", required = true)
            @PathVariable final Long height,
            @ApiParam(value = "Order of transaction in the block", required = true)
            @PathVariable final Integer tx
    ) {
        TransactionId id = new TransactionId(height, tx);

        return this.transactionRepository
                .findById(id)
                .map(TransactionResource::new)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "transaction",
                        id.toString()
                ));
    }

    @ApiOperation("List transactions in a block")
    @GetMapping("/blocks/{height:[0-9]+}/transactions")
    public Collection<TransactionResource> getTransactionsByBlock(
            @ApiParam(value = "Block height", required = true)
            @PathVariable final Long height,
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(
                    value = "offset",
                    defaultValue = "0"
            ) final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(
                    value = "limit",
                    defaultValue = "10"
            ) final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(
                    value = "sortDir",
                    defaultValue = "ASC"
            ) final Sort.Direction sortDir
    ) {
        if (limit > MAX_TRANSACTIONS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_TRANSACTIONS_PER_REQUEST);

        this.validateBlock(height);

        final OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(
                offset, limit, new Sort(sortDir, "id")
        );

        final List<TransactionResource> transactions = new ArrayList<>();
        this.transactionRepository.findByBlockHeight(height, pageRequest)
                .forEach(t -> transactions.add(new TransactionResource(t)));

        return transactions;
    }

    @ApiOperation("Count transactions in a block")
    @GetMapping("/blocks/{height:[0-9]+}/transactions/count")
    public CountResource getTransactionsByBlockCount(
            @ApiParam(value = "Block height", required = true)
            @PathVariable final Long height
    ) {
        this.validateBlock(height);

        return new CountResource(
                this.transactionRepository.countByBlockHeight(height)
        );
    }

    /**
     * Verify whether a block exists
     * If not, throw ResourceNotFoundException
     *
     * @param blockHeight Block height
     * @throws ResourceNotFoundException When there is no such block
     */
    private void validateBlock(Long blockHeight) {
        this.blockRepository.findByHeight(blockHeight).orElseThrow(() ->
                new ResourceNotFoundException("block", blockHeight)
        );
    }
}
