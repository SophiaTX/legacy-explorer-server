package com.sophiatx.explorer.api.controller.sorting;

public enum BlockSort {
    height,
    transactionCount,
    operationCount
}
