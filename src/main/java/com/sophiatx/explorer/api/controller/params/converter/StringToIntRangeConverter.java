package com.sophiatx.explorer.api.controller.params.converter;

import com.sophiatx.explorer.api.controller.params.IntRange;
import com.sophiatx.explorer.api.exception.InvalidParameterFormatException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToIntRangeConverter implements Converter<String, IntRange> {
    private static final String FORMAT = "^[0-9]+(?:-[0-9]+)?$";

    @Override
    public IntRange convert(String source)
            throws InvalidParameterFormatException,
                   NumberFormatException {
        if (!source.matches(FORMAT)) {
            throw new InvalidParameterFormatException(source, FORMAT);
        }

        String[] parts = source.split("-");

        return new IntRange(
                Integer.parseInt(parts[0]),
                Integer.parseInt(parts.length == 1 ? parts[0] : parts[1])
        );
    }
}
