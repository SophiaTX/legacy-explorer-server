package com.sophiatx.explorer.api.controller.params.converter;

import com.sophiatx.explorer.api.controller.params.ChronoLocalDateTimeRange;
import com.sophiatx.explorer.api.exception.InvalidParameterFormatException;
import com.sophiatx.explorer.helper.ISO8601;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class StringToLocalDateTimeRangerConverter
        implements Converter<String, ChronoLocalDateTimeRange> {
    private static final Pattern PATTERN = Pattern.compile("^((?:[1-9][0-9]*)?[0-9]{4}-(?:1[0-2]|0[1-9])-(?:3[01]|0[1-9]|[12][0-9])T(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9])(?:-((?:[1-9][0-9]*)?[0-9]{4}-(?:1[0-2]|0[1-9])-(?:3[01]|0[1-9]|[12][0-9])T(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]))?$");

    @Override
    public ChronoLocalDateTimeRange convert(String source)
            throws InvalidParameterFormatException,
                   DateTimeParseException {
        Matcher m = PATTERN.matcher(source);

        if (!m.find()) {
            throw new InvalidParameterFormatException(source, PATTERN.toString());
        }

        try {
            String endDate = m.group(2) == null ? m.group(1) : m.group(2);
            return new ChronoLocalDateTimeRange(
                    ISO8601.toDate(m.group(1)),
                    ISO8601.toDate(endDate)
            );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
