package com.sophiatx.explorer.api.controller;

import com.sophiatx.explorer.api.controller.sorting.AccountSort;
import com.sophiatx.explorer.api.exception.ResourceNotFoundException;
import com.sophiatx.explorer.api.exception.TooManyObjectsRequestedException;
import com.sophiatx.explorer.api.resource.AccountResource;
import com.sophiatx.explorer.api.resource.BalanceResource;
import com.sophiatx.explorer.api.resource.CountResource;
import com.sophiatx.explorer.database.OffsetBasedPageRequest;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.lang.ApiDoc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Api(tags = "Accounts", description = "Access to accounts")
@RestController
@RequestMapping(value = "/accounts", produces = {MediaType.APPLICATION_JSON_VALUE})
public class AccountController {
    private static final int MAX_ACCOUNTS_PER_REQUEST = 1000;

    private AccountRepository accountRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @ApiOperation("List accounts")
    @GetMapping()
    public Collection<AccountResource> getAccounts(
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(value = "offset", defaultValue = "0") final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(value = "limit", defaultValue = "10") final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(value = "sortDir", defaultValue = "ASC") final Sort.Direction sortDir,
            @ApiParam(ApiDoc.SORT_COL)
            @RequestParam(value = "sortBy", defaultValue = "id") final AccountSort sortBy
    ) {
        if (limit > MAX_ACCOUNTS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_ACCOUNTS_PER_REQUEST);

        final OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(
                offset, limit, new Sort(sortDir, sortBy.name())
        );

        final List<AccountResource> accounts = new ArrayList<>();
        this.accountRepository.findAll(pageRequest)
                .forEach(a -> accounts.add(new AccountResource(a)));

        return accounts;
    }

    @ApiOperation("Get number of accounts")
    @GetMapping("/count")
    public CountResource getAccountCount() {
        return new CountResource(this.accountRepository.count());
    }

    @ApiOperation("Get account by ID")
    @GetMapping("/{id:[0-9]+}")
    public AccountResource getAccount(
            @ApiParam(value = "Account ID", required = true)
            @PathVariable final Integer id
    ) {
        return this.accountRepository.findById(id)
                .map(AccountResource::new)
                .orElseThrow(() -> new ResourceNotFoundException("account", id));
    }

    @ApiOperation("Get account by name")
    @GetMapping("/{name:[a-zA-Z][a-zA-Z0-9-_]*}")
    public AccountResource getAccountByName(
            @ApiParam(value = "Account name", required = true)
            @PathVariable final String name
    ) {
        return this.accountRepository.findByName(name)
                .map(AccountResource::new)
                .orElseThrow(() -> new ResourceNotFoundException("account", name));
    }

    @ApiOperation("Get account's balances")
    @GetMapping("/{id:[0-9]+}/balances")
    public Collection<BalanceResource> getAccountBalance(
            @ApiParam(value = "Account ID", required = true)
            @PathVariable final Integer id
    ) {
        return this.accountRepository.findById(id)
                .map(a -> {
                    List<BalanceResource> balances = new ArrayList<>();
                    for (Map.Entry<Integer, Long> b : a.getBalances().entrySet()) {
                        balances.add(new BalanceResource(b.getKey(), b.getValue()));
                    }
                    return balances;
                })
                .orElseThrow(() -> new ResourceNotFoundException("account", id));
    }
}
