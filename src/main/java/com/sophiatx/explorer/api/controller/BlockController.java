package com.sophiatx.explorer.api.controller;

import com.sophiatx.explorer.api.controller.sorting.BlockSort;
import com.sophiatx.explorer.api.exception.ResourceNotFoundException;
import com.sophiatx.explorer.api.exception.TooManyObjectsRequestedException;
import com.sophiatx.explorer.api.resource.BlockResource;
import com.sophiatx.explorer.api.resource.CountResource;
import com.sophiatx.explorer.database.OffsetBasedPageRequest;
import com.sophiatx.explorer.database.model.Block;
import com.sophiatx.explorer.database.repository.BlockRepository;
import com.sophiatx.explorer.database.repository.MinerRepository;
import com.sophiatx.explorer.lang.ApiDoc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Api(tags = "Blocks", description = "Access to blocks")
@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
public class BlockController {
    private static final int MAX_BLOCKS_PER_REQUEST = 100;

    private final BlockRepository blockRepository;
    private final MinerRepository minerRepository;

    @Autowired
    public BlockController(
            BlockRepository blockRepository,
            MinerRepository minerRepository
    ) {
        this.blockRepository = blockRepository;
        this.minerRepository = minerRepository;
    }

    @ApiOperation("List blocks")
    @GetMapping("/blocks")
    public Collection<BlockResource> getBlocks(
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(
                    value = "offset",
                    defaultValue = "0"
            ) final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(
                    value = "limit",
                    defaultValue = "10"
            ) final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(
                    value = "sortDir",
                    defaultValue = "ASC"
            ) final Sort.Direction sortDir,
            @ApiParam(ApiDoc.SORT_COL)
            @RequestParam(
                    value = "sortBy",
                    defaultValue = "height"
            ) final BlockSort sortBy,
            @ApiParam("Whether to include empty blocks in the dataset")
            @RequestParam(
                    value = "emptyBlocks",
                    defaultValue = "true"
            ) final boolean emptyBlocks
    ) {
        if (limit > MAX_BLOCKS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_BLOCKS_PER_REQUEST);

        final OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(
                offset, limit, new Sort(sortDir, sortBy.name())
        );

        final List<BlockResource> blocks = new ArrayList<>();

        Page<Block> bl = emptyBlocks
                ? this.blockRepository.findAll(pageRequest)
                : this.blockRepository
                .findAllByTransactionCountGreaterThan(0, pageRequest);

        bl.forEach(block -> blocks.add(new BlockResource(block)));

        return blocks;
    }

    @ApiOperation("Get number of blocks")
    @GetMapping("/blocks/count")
    public CountResource getBlockCount() {
        return new CountResource(this.blockRepository.count());
    }

    @ApiOperation("Get block")
    @GetMapping("/blocks/{height:[0-9]+}")
    public BlockResource getBlock(
            @ApiParam(value = "Block height", required = true)
            @PathVariable final Long height
    ) {
        return this.blockRepository.findByHeight(height)
                .map(BlockResource::new)
                .orElseThrow(() -> new ResourceNotFoundException("block", height));
    }

    @ApiOperation("List blocks by miner")
    @GetMapping("/miners/{minerId:[0-9]+}/blocks")
    public Collection<BlockResource> getBlocksByMiner(
            @ApiParam(value = "Miner ID", required = true)
            @PathVariable Integer minerId,
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(
                    value = "offset",
                    defaultValue = "0"
            ) final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(
                    value = "limit",
                    defaultValue = "10"
            ) final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(
                    value = "sortDir",
                    defaultValue = "ASC"
            ) final Sort.Direction sortDir,
            @ApiParam(ApiDoc.SORT_COL)
            @RequestParam(
                    value = "sortBy",
                    defaultValue = "height"
            ) final BlockSort sortBy,
            @ApiParam("Whether to include empty blocks in the dataset")
            @RequestParam(
                    value = "emptyBlocks",
                    defaultValue = "true"
            ) final boolean emptyBlocks
    ) {
        if (limit > MAX_BLOCKS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_BLOCKS_PER_REQUEST);

        this.validateMiner(minerId);

        final OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(
                offset, limit, new Sort(sortDir, sortBy.name())
        );

        final List<BlockResource> blocks = new ArrayList<>();

        Page<Block> bl = emptyBlocks
                ? this.blockRepository.findByMinerId(minerId, pageRequest)
                : this.blockRepository
                .findByMinerIdAndTransactionCountGreaterThan(minerId, 0, pageRequest);

        bl.forEach(b -> blocks.add(new BlockResource(b)));

        return blocks;
    }

    private void validateMiner(Integer minerId) {
        this.minerRepository.findById(minerId).orElseThrow(() ->
                new ResourceNotFoundException("miner", minerId)
        );
    }
}
