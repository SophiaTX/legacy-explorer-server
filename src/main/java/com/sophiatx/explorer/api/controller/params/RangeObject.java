package com.sophiatx.explorer.api.controller.params;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

public class RangeObject<T extends Comparable<T>> {
    protected T from;
    protected T to;

    public RangeObject(T from, T to) {
        this.from = from;
        this.to = to;
    }

    public T getFrom() {
        return from;
    }

    public T getTo() {
        return to;
    }

    /**
     * Create a predicate for testing whether the second argument is
     * between the `from` and `to` values
     *
     * @param builder Criteria Builder instance
     * @param path    Attribute path from a bound type
     * @return between predicate
     */
    public Predicate toPredicate(CriteriaBuilder builder, Path<T> path) {
        if (from.compareTo(to) == 0) {
            return builder.equal(path, from);
        } else {
            return builder.between(path, from, to);
        }
    }
}
