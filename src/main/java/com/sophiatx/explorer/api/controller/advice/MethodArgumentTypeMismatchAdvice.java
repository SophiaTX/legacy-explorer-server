package com.sophiatx.explorer.api.controller.advice;

import com.sophiatx.explorer.api.resource.ErrorResource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class MethodArgumentTypeMismatchAdvice {
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorResource> handle(MethodArgumentTypeMismatchException e) {
        // Which Exception caused the argument type to be mismatched
        Throwable cause = e.getCause();
        // The type we were asking for and failed to convert to
        Class<?> type;

        // When converting to List<Enum>, individual values from the list
        // are converted to enums first. This causes ConversionFailedException
        // to be thrown if user provides us with an invalid enum value.
        // Conversion to List itself is in this case fine, but the conversion
        // of value in the list failed. We need to verify whether that's the
        // case and adjust the required type accordingly.
        if (cause instanceof ConversionFailedException) {
            ConversionFailedException ce = (ConversionFailedException) cause;
            type = ce.getTargetType().getObjectType();
        } else {
            type = e.getRequiredType();
        }

        String message = "The parameter `" + e.getName() + "` must ";

        // When the required type is enum, we want to let the client know which
        // values are accepted and whether it is a list of enums or just a
        // single-value field ( in case it is not immediately obvious ).
        // If it is not an enum, just let the client know which type to use.
        if (type.isEnum()) {
            String enums = StringUtils.join(type.getEnumConstants(), ", ");

            // If the required type in the root Exception was List and the
            // cause of failure was conversion to Enum, we know List<SomeEnum>
            // was the required type. Otherwise it was a single enum value.
            if (e.getRequiredType().getName().equals("java.util.List"))
                message += "be a list of values";
            else
                message += "be a value";

            // Inform which enum should have been used and list its values.
            message += " from enum `" + type.getSimpleName() + "` ( " + enums + " ).";
        } else {
            message += "must be of type " + type.getTypeName();
        }

        // Generate API error response
        return ResponseEntity
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .body(new ErrorResource(
                        "MethodArgumentTypeMismatch",
                        message,
                        HttpStatus.UNPROCESSABLE_ENTITY.value()
                ));
    }
}
