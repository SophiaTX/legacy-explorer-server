package com.sophiatx.explorer.api.controller.params;

public enum LogicalOperator {
    AND,
    OR
}
