package com.sophiatx.explorer.api.controller.params;

import java.time.chrono.ChronoLocalDateTime;

public class ChronoLocalDateTimeRange extends RangeObject<ChronoLocalDateTime<?>> {
    public ChronoLocalDateTimeRange(
            ChronoLocalDateTime<?> from,
            ChronoLocalDateTime<?> to
    ) {
        super(from, to);
    }
}
