package com.sophiatx.explorer.api.controller.params;

public class LongRange extends RangeObject<Long> {
    public LongRange(Long from, Long to) {
        super(from, to);
    }
}
