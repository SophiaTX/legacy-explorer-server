package com.sophiatx.explorer.api.controller;

import com.sophiatx.explorer.api.controller.sorting.MinerSort;
import com.sophiatx.explorer.api.exception.ResourceNotFoundException;
import com.sophiatx.explorer.api.exception.TooManyObjectsRequestedException;
import com.sophiatx.explorer.api.resource.CountResource;
import com.sophiatx.explorer.api.resource.MinerResource;
import com.sophiatx.explorer.database.OffsetBasedPageRequest;
import com.sophiatx.explorer.database.repository.MinerRepository;
import com.sophiatx.explorer.lang.ApiDoc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Api(tags = "Miners", description = "Access to miners")
@RestController
@RequestMapping(value = "/miners", produces = {MediaType.APPLICATION_JSON_VALUE})
public class MinerController {
    private static final int MAX_MINERS_PER_REQUEST = 1000;

    private MinerRepository minerRepository;

    @Autowired
    public MinerController(MinerRepository minerRepository) {
        this.minerRepository = minerRepository;
    }

    @ApiOperation("List miners")
    @GetMapping
    public Collection<MinerResource> getMiners(
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(value = "offset", defaultValue = "0") final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(value = "limit", defaultValue = "10") final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(value = "sortDir", defaultValue = "ASC") final Sort.Direction sortDir,
            @ApiParam(ApiDoc.SORT_COL)
            @RequestParam(value = "sortBy", defaultValue = "id") final MinerSort sortBy
    ) {
        if (limit > MAX_MINERS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_MINERS_PER_REQUEST);

        final OffsetBasedPageRequest pageRequest = new OffsetBasedPageRequest(
                offset, limit, new Sort(sortDir, sortBy.name())
        );

        final List<MinerResource> miners = new ArrayList<>();
        this.minerRepository.findAll(pageRequest)
                .forEach(m -> miners.add(new MinerResource(m)));

        return miners;
    }

    @ApiOperation("Get number of miners")
    @GetMapping("/count")
    public CountResource getMinerCount() {
        return new CountResource(this.minerRepository.count());
    }

    @ApiOperation("Get miner")
    @GetMapping("/{id:[0-9]+}")
    public MinerResource getMiner(
            @ApiParam(value = "Miner ID", required = true)
            @PathVariable Integer id
    ) {
        return this.minerRepository.findById(id)
                .map(MinerResource::new)
                .orElseThrow(() -> new ResourceNotFoundException("miner", id));
    }
}
