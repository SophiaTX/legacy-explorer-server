package com.sophiatx.explorer.api.controller;

import com.sophiatx.explorer.api.controller.params.LogicalOperator;
import com.sophiatx.explorer.api.controller.params.LongRange;
import com.sophiatx.explorer.api.exception.ResourceNotFoundException;
import com.sophiatx.explorer.api.exception.TooManyObjectsRequestedException;
import com.sophiatx.explorer.api.resource.CountResource;
import com.sophiatx.explorer.api.resource.OperationResource;
import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.OffsetBasedPageRequest;
import com.sophiatx.explorer.database.id.OperationId;
import com.sophiatx.explorer.database.id.TransactionId;
import com.sophiatx.explorer.database.model.Operation;
import com.sophiatx.explorer.database.repository.AccountRepository;
import com.sophiatx.explorer.database.repository.OperationRepository;
import com.sophiatx.explorer.lang.ApiDoc;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Api(tags = "Operations", description = "Access to operations")
@RestController
@RequestMapping(value = "/operations", produces = {MediaType.APPLICATION_JSON_VALUE})
public class OperationController {
    private static final int MAX_OPERATIONS_PER_REQUEST = 1000;

    private EntityManager em;
    private CriteriaBuilder builder;
    private OperationRepository opRepo;
    private AccountRepository accountRepo;

    @Autowired
    public OperationController(
            EntityManager em,
            OperationRepository opRepo,
            AccountRepository accountRepo
    ) {
        this.em = em;
        this.builder = em.getCriteriaBuilder();
        this.opRepo = opRepo;
        this.accountRepo = accountRepo;
    }

    @ApiOperation("Get operation by composite ID")
    @GetMapping("/B{height:[0-9]+}T{tx:[0-9]+}O{op:[0-9]+}")
    public OperationResource getOperation(
            @ApiParam(value = "Block height", required = true)
            @PathVariable final Long height,
            @ApiParam(value = "Order of transaction in the block", required = true)
            @PathVariable final Integer tx,
            @ApiParam(value = "Order of operation in the transaction", required = true)
            @PathVariable final Integer op
    ) {
        OperationId id = new OperationId(new TransactionId(height, tx), op);
        return opRepo.findById(id)
                .map(OperationResource::new)
                .orElseThrow(() -> new ResourceNotFoundException(
                        "operation",
                        id.toString()
                ));
    }

    @ApiOperation("List operations")
    @GetMapping
    public Collection<OperationResource> getOperations(
            @ApiParam(ApiDoc.OFFSET)
            @RequestParam(
                    value = "offset", defaultValue = "0"
            ) final Integer offset,
            @ApiParam(ApiDoc.LIMIT)
            @RequestParam(
                    value = "limit", defaultValue = "10"
            ) final Integer limit,
            @ApiParam(ApiDoc.SORT_DIR)
            @RequestParam(
                    value = "sortDir", defaultValue = "ASC"
            ) final Sort.Direction sortDir,
            @ApiParam("List of operation types")
            @RequestParam(
                    value = "type", required = false
            ) final List<OperationType> opTypes,
            @ApiParam("Sender account ID")
            @RequestParam(
                    value = "senderId", required = false
            ) final Integer senderId,
            @ApiParam("Target account ID")
            @RequestParam(
                    value = "targetId", required = false
            ) final Integer targetId,
            @ApiParam("Logical operator to use on sender and target")
            @RequestParam(
                    value = "operator", defaultValue = "AND"
            ) final LogicalOperator operator,
            @ApiParam("Block height or range ( from-to )")
            @RequestParam(
                    value = "blockHeight", required = false
            ) final LongRange blockHeight
    ) {
        if (limit > MAX_OPERATIONS_PER_REQUEST)
            throw new TooManyObjectsRequestedException(limit, MAX_OPERATIONS_PER_REQUEST);

        validateAccounts(senderId, targetId);

        List<OperationResource> operations = new ArrayList<>();

        // only run the more expensive code when needed
        if (opTypes != null || senderId != null || targetId != null || blockHeight != null) {
            CriteriaQuery<Operation> query = buildSelectCriteriaQuery(
                    senderId, targetId, operator, blockHeight, opTypes, sortDir
            );

            em.createQuery(query)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList()
                    .forEach(o -> operations.add(new OperationResource(o)));
        } else {
            opRepo.findAll(new OffsetBasedPageRequest(
                    offset, limit, new Sort(sortDir, "id")
            )).forEach(
                    o -> operations.add(new OperationResource(o))
            );
        }

        return operations;
    }

    @ApiParam("Count operations in the blockchain")
    @GetMapping("/count")
    public CountResource getOperationsCount(
            @ApiParam("List of operation types")
            @RequestParam(
                    value = "type", required = false
            ) final List<OperationType> opTypes,
            @ApiParam("Sender account ID")
            @RequestParam(
                    value = "senderId", required = false
            ) final Integer senderId,
            @ApiParam("Target account ID")
            @RequestParam(
                    value = "targetId", required = false
            ) final Integer targetId,
            @ApiParam("Logical operator to use on sender and target")
            @RequestParam(
                    value = "operator", defaultValue = "AND"
            ) final LogicalOperator operator,
            @ApiParam("Block height or range ( from-to )")
            @RequestParam(
                    value = "blockHeight", required = false
            ) final LongRange blockHeight
    ) {
        validateAccounts(senderId, targetId);

        Long count;

        // only run the more expensive code when needed
        if (opTypes != null || senderId != null || targetId != null || blockHeight != null) {
            CriteriaQuery<Long> query = buildCountCriteriaQuery(
                    senderId, targetId, operator, blockHeight, opTypes
            );

            count = em.createQuery(query).getSingleResult();
        } else {
            count = opRepo.count();
        }

        return new CountResource(count);
    }

    /**
     * Validate whether accounts provided to operations filter actually exist
     *
     * @param senderId (Optional) sender account id
     * @param targetId (Optional) target account id
     */
    private void validateAccounts(Integer senderId, Integer targetId) {
        if (senderId != null) {
            this.accountRepo.findById(senderId).orElseThrow(
                    () -> new ResourceNotFoundException("account", senderId)
            );
        }

        if (targetId != null) {
            this.accountRepo.findById(targetId).orElseThrow(
                    () -> new ResourceNotFoundException("account", targetId)
            );
        }
    }

    /**
     * Build a criteria query used to retrieve the operations
     * that meet a predicates built according to the api consumer parameters
     *
     * @param senderId        Sender account ID
     * @param targetId        Target account ID
     * @param logicalOperator Logical operator ( AND/OR )
     * @param opTypes         Operation type
     * @param sortDir         Sort direction
     * @return Criteria Query that will return sorted operations
     */
    private CriteriaQuery<Operation> buildSelectCriteriaQuery(
            Integer senderId,
            Integer targetId,
            LogicalOperator logicalOperator,
            LongRange blockHeight,
            List<OperationType> opTypes,
            Sort.Direction sortDir
    ) {
        CriteriaQuery<Operation> query = builder.createQuery(Operation.class);
        Root<Operation> root = query.from(Operation.class);

        query.where(buildPredicates(
                senderId, targetId, logicalOperator, blockHeight, opTypes, root
        ));

        if (sortDir != null) {
            query.orderBy(
                    sortDir == Sort.Direction.ASC
                            ? builder.asc(root.get("id"))
                            : builder.desc(root.get("id"))
            );
        }

        return query;
    }

    /**
     * Build a criteria query used to count the number of operations
     * that meet a predicates built according to the api consumer parameters
     *
     * @param senderId        Sender account ID
     * @param targetId        Target account ID
     * @param logicalOperator Logical operator ( AND/OR )
     * @param opTypes         Operation type
     * @return Criteria Query that will return number of rows
     */
    private CriteriaQuery<Long> buildCountCriteriaQuery(
            Integer senderId,
            Integer targetId,
            LogicalOperator logicalOperator,
            LongRange blockHeight,
            List<OperationType> opTypes
    ) {
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Operation> root = query.from(Operation.class);

        query.select(builder.count(root));
        query.where(buildPredicates(
                senderId, targetId, logicalOperator, blockHeight, opTypes, root
        ));

        return query;
    }

    /**
     * Build a query predicates containing operation type and account IDs
     *
     * @param opTypes  Operation type
     * @param senderId Sender account ID
     * @param targetId Target account ID
     * @param operator Logical operator ( AND/OR )
     * @param root     Root for Operation class
     * @return Predicates[]
     */
    private Predicate[] buildPredicates(
            Integer senderId,
            Integer targetId,
            LogicalOperator operator,
            LongRange blockHeight,
            List<OperationType> opTypes,
            Root<Operation> root
    ) {
        List<Predicate> predicates = new ArrayList<>();
        List<Predicate> orPredicates = new ArrayList<>();

        // ** convenience variables **
        boolean hasSender = senderId != null;
        boolean hasTarget = targetId != null;
        // note: we only use "OR" operator when requested by the API consumer
        //       and both sender and target IDs are provided ( not null )
        boolean useOr = operator == LogicalOperator.OR && hasSender && hasTarget;
        boolean hasOt = opTypes != null && opTypes.size() != 0;
        boolean allOtSelected = hasOt && opTypes.size() == OperationType.size();

        // there is no reason to use operation filter when API consumer selected
        // none ( = no filter ) or all ( = no reason to filter ) operation types.
        if (hasOt && !allOtSelected)
            predicates.add(root.get("type").in(opTypes));

        if (blockHeight != null) {
            predicates.add(blockHeight.toPredicate(builder, root.get("block")));
        }

        if (hasSender) {
            Predicate predicate = builder.equal(root.get("sender"), senderId);
            if (useOr)
                orPredicates.add(predicate);
            else
                predicates.add(predicate);
        }

        if (hasTarget) {
            Predicate predicate = builder.equal(root.get("target"), targetId);
            if (useOr)
                orPredicates.add(predicate);
            else
                predicates.add(predicate);
        }

        if (useOr)
            predicates.add(builder.or(orPredicates.toArray(new Predicate[0])));

        return predicates.toArray(new Predicate[0]);
    }
}
