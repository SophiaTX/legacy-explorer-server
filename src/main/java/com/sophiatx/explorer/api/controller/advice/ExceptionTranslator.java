package com.sophiatx.explorer.api.controller.advice;

import com.sophiatx.explorer.api.resource.ErrorResource;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionTranslator {
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorResource> handle(RuntimeException e) {
        ErrorResource errorResource;

        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(
                e.getClass(),
                ResponseStatus.class
        );

        //noinspection ConstantConditions ( .findAnnotations may return null )
        errorResource = new ErrorResource(
                e.getClass().getSimpleName().replace("Exception", ""),
                e.getMessage(),
                responseStatus != null
                        ? responseStatus.value().value()
                        : HttpStatus.INTERNAL_SERVER_ERROR.value()
        );

        return ResponseEntity
                .status(errorResource.errorCode)
                .body(errorResource);
    }
}
