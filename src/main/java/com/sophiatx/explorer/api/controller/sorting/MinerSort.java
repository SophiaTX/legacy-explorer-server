package com.sophiatx.explorer.api.controller.sorting;

public enum MinerSort {
    id,
    totalVotes,
    totalMissed,
    totalMined
}
