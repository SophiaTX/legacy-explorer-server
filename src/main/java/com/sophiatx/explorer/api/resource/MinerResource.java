package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.sophiatx.explorer.database.model.Miner;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Miner")
@JsonRootName("miner")
public class MinerResource {
    @ApiModelProperty("Miner ID")
    public final Integer id;

    @ApiModelProperty("Total number of votes the miner received from token holders")
    public final Long totalVotes;

    @ApiModelProperty("Total number of blocks the miner failed to create")
    public final Long totalMissed;

    @ApiModelProperty("Total number of blocks the miner created")
    public final Long totalMined;

    @ApiModelProperty("Account the miner belongs to")
    public final AccountResource account;

    public MinerResource(Miner miner) {
        this.id = miner.getId();
        this.totalVotes = miner.getTotalVotes();
        this.totalMissed = miner.getTotalMissed();
        this.totalMined = miner.getTotalMined();
        this.account = new AccountResource(miner.getAccount());
    }
}
