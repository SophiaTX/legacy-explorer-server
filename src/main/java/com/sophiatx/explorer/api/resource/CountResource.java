package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.annotations.ApiModel;

@ApiModel("Count")
@JsonRootName("count")
public class CountResource {
    public final Long count;

    public CountResource(Long count) {
        this.count = count;
    }
}
