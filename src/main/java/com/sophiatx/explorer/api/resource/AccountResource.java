package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.sophiatx.explorer.database.model.Account;
import com.sophiatx.explorer.database.model.Miner;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Account")
@JsonRootName("account")
public class AccountResource {
    @ApiModelProperty("Account ID")
    public final Integer id;

    @ApiModelProperty("Account name")
    public final String name;

    @ApiModelProperty("ID of an account that created this account")
    public final Integer registrarId;

    @ApiModelProperty("ID of miner that belongs to this account")
    public final Integer minerId;

    @ApiModelProperty("SPHTX balance")
    public final Long mainBalance;

    public AccountResource(Account account) {
        this.id = account.getId();
        this.name = account.getName();
        this.mainBalance = account.getMainBalance();

        this.registrarId = account.getRegistrar()
                .map(Account::getId)
                .orElse(null);

        this.minerId = account.getMiner()
                .map(Miner::getId)
                .orElse(null);
    }
}
