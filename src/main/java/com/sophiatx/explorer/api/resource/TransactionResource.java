package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.sophiatx.explorer.database.id.TransactionId;
import com.sophiatx.explorer.database.model.Transaction;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.List;

@ApiModel("Transaction")
@JsonRootName("transaction")
public class TransactionResource {
    @ApiModelProperty("Transaction composite ID")
    public final String id;

    @ApiModelProperty("Height of a block the transaction is stored in")
    public final Long blockHeight;

    @ApiModelProperty("Referenced block")
    public final Long refBlockNum;

    @ApiModelProperty("Referenced block prefix")
    public final Long refBlockPrefix;

    @ApiModelProperty("Signatures")
    public final List<String> signatures;

    @ApiModelProperty("Operations executed in the transaction")
    public final List<OperationResource> operations;

    @ApiModelProperty("Order of the transaction in a block")
    public final Integer orderInBlock;

    public TransactionResource(Transaction transaction) {
        this.id = transaction.getId().toString();
        this.blockHeight = transaction.getId().getBlockHeight();
        this.orderInBlock = transaction.getId().getOrderInBlock();
        this.refBlockNum = transaction.getRefBlockNum();
        this.refBlockPrefix = transaction.getRefBlockPrefix();
        this.signatures = transaction.getSignatures();
        this.operations = new ArrayList<>();

        transaction.getOperations().forEach(
                op -> this.operations.add(new OperationResource(op))
        );
    }
}
