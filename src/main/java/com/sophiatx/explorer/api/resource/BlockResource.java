package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.sophiatx.explorer.database.model.Block;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

@ApiModel("Block")
@JsonRootName("block")
public class BlockResource {
    @ApiModelProperty("Block height")
    public final Long height;

    @ApiModelProperty("ISO8601 timestamp describing when the block was created")
    public final LocalDateTime time;

    @ApiModelProperty("Miner that created the block")
    public final MinerResource miner;

    @ApiModelProperty("Miner's signature")
    public final String minerSignature;

    @ApiModelProperty("Total number of transactions in the block")
    public final Integer transactionCount;

    @ApiModelProperty("Total number of operations in the block")
    public final Integer operationCount;

    public BlockResource(final Block block) {
        this.height = block.getHeight();
        this.time = block.getTime();
        this.miner = new MinerResource(block.getMiner());
        this.minerSignature = block.getMinerSignature();
        this.transactionCount = block.getTransactionCount();
        this.operationCount = block.getOperationCount();
    }
}
