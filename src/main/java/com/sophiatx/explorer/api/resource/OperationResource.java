package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.model.Operation;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Operation")
@JsonRootName("operation")
public class OperationResource {
    @ApiModelProperty("Operation composite ID")
    public final String id;

    @ApiModelProperty("Type of operation")
    public final OperationType type;

    @ApiModelProperty("JSON-encoded operation data")
    @JsonRawValue
    public final String data;

    @ApiModelProperty("Order of operation in the parent transaction")
    public final Integer orderInTX;

    @ApiModelProperty("An account that sent the operation to the blockchain")
    public final AccountResource sender;

    @ApiModelProperty("An account the operation was targeted to")
    public final AccountResource target;

    @ApiModelProperty("Height of a block the operation is stored in")
    public final Long blockHeight;

    @ApiModelProperty("Order of parent transaction in the block")
    public final Integer orderOfTxInBlock;

    public OperationResource(Operation operation) {
        this.id = operation.getId().toString();
        this.type = operation.getType();
        this.data = operation.getData().toString();
        this.orderInTX = operation.getId().getOrderInTransaction();
        this.sender = new AccountResource(operation.getSender());
        this.target = new AccountResource(operation.getTarget());
        this.blockHeight = operation.getId().getTransactionId().getBlockHeight();
        this.orderOfTxInBlock = operation.getId().getTransactionId().getOrderInBlock();
    }
}
