package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Balance")
@JsonRootName("balance")
public class BalanceResource {
    @ApiModelProperty("ID of resource")
    public final Integer resourceId;

    @ApiModelProperty("Amount of resource")
    public final Long amount;

    public BalanceResource(Integer resourceId, Long amount) {
        this.resourceId = resourceId;
        this.amount = amount;
    }
}
