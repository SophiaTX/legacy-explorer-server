package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.sophiatx.explorer.blockchain.model.DynamicGlobalProperties;
import io.swagger.annotations.ApiModel;

import java.time.LocalDateTime;

@ApiModel("DynamicGlobalProperties")
@JsonRootName("dynamicGlobalProperties")
public class DynamicGlobalPropertiesResource {
    public final Long headBlockNumber;
    public final String headBlockId;
    public final LocalDateTime time;
    public final String currentMiner;
    public final LocalDateTime nextMaintenanceTime;
    public final LocalDateTime lastBudgetTime;
    public final Long unspentFeeBudget;
    public final Long minedRewards;
    public final Long minerBudgetFromFees;
    public final Long minerBudgetFromRewards;
    public final Integer accountsRegisteredThisInterval;
    public final Integer recentlyMissedCount;
    public final Integer currentAslot;
    public final String recentSlotsFilled;
    public final Integer dynamicFlags;
    public final Long lastIrreversibleBlockNum;

    public DynamicGlobalPropertiesResource(DynamicGlobalProperties dgp) {
        this.headBlockNumber = dgp.getHeadBlockNumber();
        this.headBlockId = dgp.getHeadBlockId();
        this.time = dgp.getTime();
        this.currentMiner = dgp.getCurrentMiner();
        this.nextMaintenanceTime = dgp.getNextMaintenanceTime();
        this.lastBudgetTime = dgp.getLastBudgetTime();
        this.unspentFeeBudget = dgp.getUnspentFeeBudget();
        this.minedRewards = dgp.getMinedRewards();
        this.minerBudgetFromFees = dgp.getMinerBudgetFromFees();
        this.minerBudgetFromRewards = dgp.getMinerBudgetFromRewards();
        this.accountsRegisteredThisInterval = dgp.getAccountsRegisteredThisInterval();
        this.recentlyMissedCount = dgp.getRecentlyMissedCount();
        this.currentAslot = dgp.getCurrentAslot();
        this.recentSlotsFilled = dgp.getRecentSlotsFilled();
        this.dynamicFlags = dgp.getDynamicFlags();
        this.lastIrreversibleBlockNum = dgp.getLastIrreversibleBlockNum();
    }
}
