package com.sophiatx.explorer.api.resource;

import com.fasterxml.jackson.annotation.JsonRootName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Error")
@JsonRootName("error")
public class ErrorResource {
    @ApiModelProperty("Name of the exception that was thrown")
    public final String exception;

    @ApiModelProperty("Short text message describing what went wrong")
    public final String message;

    @ApiModelProperty("HTTP error code")
    public final Integer errorCode;

    public ErrorResource(String exception, String message, Integer errorCode) {
        this.exception = exception;
        this.message = message;
        this.errorCode = errorCode;
    }
}
