package com.sophiatx.explorer.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TooManyObjectsRequestedException extends RuntimeException {
    public TooManyObjectsRequestedException(Integer requested, Integer limit) {
        super("You have requested too many blocks (" + requested + "). " +
                "Maximum allowed blocks per request: " + limit);
    }
}
