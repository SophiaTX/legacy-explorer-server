package com.sophiatx.explorer.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    /**
     * @param resource   Type / name of resource that was requested
     * @param identifier Identification of resource that was provided
     */
    public ResourceNotFoundException(String resource, String identifier) {
        super("Requested " + resource + " \"" + identifier + "\" was not found");
    }

    /**
     * @param resource   Type / name of resource that was requested
     * @param identifier Identification of resource that was provided
     */
    public ResourceNotFoundException(String resource, Integer identifier) {
        super("Requested " + resource + " " + identifier + " was not found");
    }

    /**
     * @param resource   Type / name of resource that was requested
     * @param identifier Identification of resource that was provided
     */
    public ResourceNotFoundException(String resource, Long identifier) {
        super("Requested " + resource + " " + identifier + " was not found");
    }

    /**
     * @param resource Type / name of resource that was requested
     */
    public ResourceNotFoundException(String resource) {
        super("Requested resource of type " + resource + " was not found");
    }

    public ResourceNotFoundException() {
        super("Requested resource was not found");
    }
}
