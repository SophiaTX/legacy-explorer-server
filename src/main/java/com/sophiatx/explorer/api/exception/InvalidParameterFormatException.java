package com.sophiatx.explorer.api.exception;

public class InvalidParameterFormatException extends IllegalArgumentException {
    public InvalidParameterFormatException(String input) {
        super("Input string ( '" + input + "' ) is invalid");
    }

    public InvalidParameterFormatException(String input, String pattern) {
        super("Input string ( '" + input + "' ) does not match required " +
                "format: " + pattern);
    }
}
