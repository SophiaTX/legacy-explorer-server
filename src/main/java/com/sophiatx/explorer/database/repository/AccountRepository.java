package com.sophiatx.explorer.database.repository;

import com.sophiatx.explorer.database.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {
    Optional<Account> findFirst1ByOrderByIdDesc();

    Optional<Account> findById(Integer id);

    Optional<Account> findByName(String name);
}
