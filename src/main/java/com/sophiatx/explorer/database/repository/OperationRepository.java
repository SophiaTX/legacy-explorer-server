package com.sophiatx.explorer.database.repository;

import com.sophiatx.explorer.database.id.OperationId;
import com.sophiatx.explorer.database.model.Operation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OperationRepository extends JpaRepository<Operation, Long> {
    Optional<Operation> findById(OperationId id);

    @Query(
            nativeQuery = true,
            value = "SELECT COUNT(o.id) FROM operations AS o " +
                    "LEFT JOIN transactions t ON t.id = o.transaction_id " +
                    "WHERE t.block_height = ?1"
    )
    Long countByBlockHeight(Long height);
}
