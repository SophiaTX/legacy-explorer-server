package com.sophiatx.explorer.database.repository;

import com.sophiatx.explorer.database.id.TransactionId;
import com.sophiatx.explorer.database.model.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    Optional<Transaction> findById(TransactionId id);

    List<Transaction> findByBlockHeight(
            Long blockHeight, Pageable page
    );

    Long countByBlockHeight(Long blockHeight);
}
