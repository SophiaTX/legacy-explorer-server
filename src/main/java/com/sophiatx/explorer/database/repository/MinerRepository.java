package com.sophiatx.explorer.database.repository;

import com.sophiatx.explorer.database.model.Miner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MinerRepository extends JpaRepository<Miner, Integer> {
    Optional<Miner> findById(Integer id);

    Optional<Miner> findFirst1ByOrderByIdDesc();
}
