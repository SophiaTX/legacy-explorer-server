package com.sophiatx.explorer.database.repository;

import com.sophiatx.explorer.database.model.JsonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JsonEntityRepository extends JpaRepository<JsonEntity, String> {
    public Optional<JsonEntity> findByKey(String key);
}
