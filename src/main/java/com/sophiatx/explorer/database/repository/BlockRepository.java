package com.sophiatx.explorer.database.repository;

import com.sophiatx.explorer.database.model.Block;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BlockRepository extends JpaRepository<Block, Long> {
    Page<Block> findAllByTransactionCountGreaterThan(Integer txCnt, Pageable page);

    Optional<Block> findFirst1ByOrderByHeightDesc();

    Optional<Block> findByHeight(Long height);

    Page<Block> findByMinerId(Integer minerId, Pageable page);

    Page<Block> findByMinerIdAndTransactionCountGreaterThan(
            Integer minerId,
            Integer txCount,
            Pageable page
    );
}
