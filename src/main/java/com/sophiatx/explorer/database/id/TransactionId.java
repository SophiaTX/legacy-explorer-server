package com.sophiatx.explorer.database.id;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class TransactionId implements Serializable {
    @Column(name = "block_height", nullable = false)
    private Long blockHeight;

    @Column(name = "order_in_block", nullable = false)
    private Integer orderInBlock;

    public TransactionId() {
    }

    public TransactionId(Long blockHeight, Integer orderInBlock) {
        this.blockHeight = blockHeight;
        this.orderInBlock = orderInBlock;
    }

    public String toString() {
        return "B" + blockHeight + "T" + orderInBlock;
    }

    public Long getBlockHeight() {
        return blockHeight;
    }

    public TransactionId setBlockHeight(Long blockHeight) {
        this.blockHeight = blockHeight;
        return this;
    }

    public Integer getOrderInBlock() {
        return orderInBlock;
    }

    public TransactionId setOrderInBlock(Integer orderInBlock) {
        this.orderInBlock = orderInBlock;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionId that = (TransactionId) o;

        return (blockHeight.equals(that.blockHeight) &&
                orderInBlock.equals(that.orderInBlock)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(blockHeight, orderInBlock);
    }
}
