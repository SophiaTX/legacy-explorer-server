package com.sophiatx.explorer.database.id;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OperationId implements Serializable {
    private TransactionId transactionId;

    @Column(name = "order_in_tx")
    private Integer orderInTransaction;

    public OperationId() {
    }

    public OperationId(TransactionId transactionId, Integer orderInTransaction) {
        this.transactionId = transactionId;
        this.orderInTransaction = orderInTransaction;
    }

    public String toString() {
        return "B" + transactionId.getBlockHeight()
                + "T" + transactionId.getOrderInBlock()
                + "O" + orderInTransaction;
    }

    public TransactionId getTransactionId() {
        return transactionId;
    }

    public OperationId setTransactionId(TransactionId transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public Integer getOrderInTransaction() {
        return orderInTransaction;
    }

    public OperationId setOrderInTransaction(Integer orderInTransaction) {
        this.orderInTransaction = orderInTransaction;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OperationId that = (OperationId) o;

        return (orderInTransaction.equals(that.orderInTransaction) &&
                transactionId.equals(that.transactionId)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId, orderInTransaction);
    }
}
