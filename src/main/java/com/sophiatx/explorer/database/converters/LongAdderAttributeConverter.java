package com.sophiatx.explorer.database.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.concurrent.atomic.LongAdder;

@Converter(autoApply = true)
public class LongAdderAttributeConverter
        implements AttributeConverter<LongAdder, Long> {
    @Override
    public Long convertToDatabaseColumn(LongAdder attribute) {
        return attribute.sum();
    }

    @Override
    public LongAdder convertToEntityAttribute(Long dbData) {
        LongAdder adder = new LongAdder();
        adder.add(dbData);
        return adder;
    }
}
