package com.sophiatx.explorer.database.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "blocks", indexes = {
        @Index(name = "i_transaction_count", columnList = "transactionCount"),
        @Index(name = "i_operation_count", columnList = "operationCount")
})
public class Block {
    @Id
    @Column(nullable = false)
    private Long height;

    @Column(columnDefinition = "TIMESTAMP")
    private LocalDateTime time;

    @ManyToOne
    @JoinColumn(name = "miner_id", referencedColumnName = "id", nullable = false)
    private Miner miner;

    @Column(columnDefinition = "CHAR(130)")
    private String minerSignature;

    @OneToMany(
            mappedBy = "block",
            targetEntity = Transaction.class,
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    private List<Transaction> transactions;

    @Column(nullable = false)
    private Integer transactionCount;

    @Column(nullable = false)
    private Integer operationCount;

    public Block() {
        this.transactions = new ArrayList<>();
    }

    public Block(
            Long height,
            LocalDateTime time,
            Miner miner,
            String minerSignature
    ) {
        this.height = height;
        this.time = time;
        this.miner = miner;
        this.minerSignature = minerSignature;
        this.transactions = new ArrayList<>();
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Miner getMiner() {
        return miner;
    }

    public Block setMiner(Miner miner) {
        this.miner = miner;
        return this;
    }

    public String getMinerSignature() {
        return minerSignature;
    }

    public void setMinerSignature(String minerSignature) {
        this.minerSignature = minerSignature;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public Block addTransaction(Transaction transaction) {
        transaction.getId().setBlockHeight(this.getHeight());
        this.transactions.add(transaction);
        return this;
    }

    public Integer getTransactionCount() {
        return transactionCount;
    }

    public Block setTransactionCount(Integer transactionCount) {
        this.transactionCount = transactionCount;
        return this;
    }

    public Integer getOperationCount() {
        return operationCount;
    }

    public Block setOperationCount(Integer operationCount) {
        this.operationCount = operationCount;
        return this;
    }
}
