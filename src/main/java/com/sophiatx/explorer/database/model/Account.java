package com.sophiatx.explorer.database.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sophiatx.explorer.database.type.JSONObjectType;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Optional;

@Entity
@Table(name = "accounts", indexes = {
        @Index(name = "i_balance", columnList = "mainBalance")
})
@TypeDefs({
        @TypeDef(name = "JSONObject", typeClass = JSONObjectType.class)
})
public class Account {
    @Id
    private Integer id;

    @Column(nullable = false, unique = true)
    private String name;

    @ManyToOne
    @JoinColumn(name = "registrar_id", referencedColumnName = "id")
    private Account registrar;

    @JsonIgnore
    @OneToOne(mappedBy = "account", targetEntity = Miner.class)
    private Miner miner;

    @ElementCollection(fetch = FetchType.LAZY)
    @JoinTable(name = "balances", joinColumns = @JoinColumn(name = "account_id"))
    @MapKeyColumn(name = "asset_id")
    @Column(name = "balance", nullable = false)
    private Map<Integer, @NotNull Long> balances;

    // SophiaTX main coin balance, this column is indexed
    private Long mainBalance;

    public Account() {
    }

    public Account(Integer id, String name, Account registrar) {
        this.id = id;
        this.name = name;
        this.registrar = registrar;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Account setName(String name) {
        this.name = name;
        return this;
    }

    public Optional<Account> getRegistrar() {
        return registrar == null ? Optional.empty() : Optional.of(registrar);
    }

    public Account setRegistrar(Account registrar) {
        this.registrar = registrar;
        return this;
    }

    public Optional<Miner> getMiner() {
        return miner == null ? Optional.empty() : Optional.of(miner);
    }

    public Account setMiner(Miner miner) {
        this.miner = miner;
        return this;
    }

    public Map<Integer, Long> getBalances() {
        return balances;
    }

    public Account setBalances(Map<Integer, Long> balances) {
        mainBalance = balances.getOrDefault(0, 0L);

        this.balances = balances;
        return this;
    }

    public Long getMainBalance() {
        return mainBalance;
    }
}
