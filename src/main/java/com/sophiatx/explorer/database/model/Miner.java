package com.sophiatx.explorer.database.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.LongAdder;

@Entity
@Table(name = "miners", indexes = {
        @Index(name = "i_total_votes", columnList = "totalVotes"),
        @Index(name = "i_total_missed", columnList = "totalMissed"),
        @Index(name = "i_total_mined", columnList = "totalMined")
})
public class Miner {
    @Id
    private Integer id;

    private Long totalVotes;

    private Long totalMissed;

    private LongAdder totalMined;

    @JsonIgnore
    @OneToMany(
            mappedBy = "miner",
            targetEntity = Block.class,
            fetch = FetchType.LAZY
    )
    private List<Block> minedBlocks;

    @OneToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false)
    private Account account;

    public Miner() {
        this.minedBlocks = new ArrayList<>();
    }

    public Miner(
            Integer id,
            Long totalVotes,
            Long totalMissed,
            Account account
    ) {
        this.id = id;
        this.totalVotes = totalVotes;
        this.totalMissed = totalMissed;
        this.account = account;
        this.minedBlocks = new ArrayList<>();
        this.totalMined = new LongAdder();
    }

    public Integer getId() {
        return id;
    }

    public Long getTotalVotes() {
        return totalVotes;
    }

    public Miner setTotalVotes(Long totalVotes) {
        this.totalVotes = totalVotes;
        return this;
    }

    public Long getTotalMissed() {
        return totalMissed;
    }

    public Miner setTotalMissed(Long totalMissed) {
        this.totalMissed = totalMissed;
        return this;
    }

    public List<Block> getMinedBlocks() {
        return minedBlocks;
    }

    public Account getAccount() {
        return account;
    }

    public Miner setAccount(Account account) {
        this.account = account;
        return this;
    }

    public Miner incrementMinedBlocksCount() {
        this.totalMined.increment();
        return this;
    }

    public Long getTotalMined() {
        return this.totalMined.sum();
    }
}
