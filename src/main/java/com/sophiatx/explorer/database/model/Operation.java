package com.sophiatx.explorer.database.model;

import com.sophiatx.explorer.blockchain.enums.OperationType;
import com.sophiatx.explorer.database.id.OperationId;
import com.sophiatx.explorer.database.type.JSONObjectType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import javax.persistence.*;

@Entity
@Table(name = "operations", indexes = {
        @Index(name = "i_type", columnList = "type")
})
@TypeDefs({
        @TypeDef(name = "JSONObject", typeClass = JSONObjectType.class)
})
public class Operation {
    @EmbeddedId
    private OperationId id;

    @Column(name = "type", columnDefinition = "SMALLINT")
    private OperationType type;

    @Type(type = "JSONObject")
    @Column(name = "data", columnDefinition = "JSONB")
    private JSONObject data;

    @ManyToOne
    @JoinColumn(name = "sender_account_id", referencedColumnName = "id")
    private Account sender;

    @ManyToOne
    @JoinColumn(name = "target_account_id", referencedColumnName = "id")
    private Account target;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(
                    name = "block_height",
                    referencedColumnName = "block_height",
                    insertable = false,
                    updatable = false
            ),
            @JoinColumn(
                    name = "order_in_block",
                    referencedColumnName = "order_in_block",
                    insertable = false,
                    updatable = false
            )
    })
    private Transaction transaction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "block_height",
            referencedColumnName = "height",
            insertable = false,
            updatable = false
    )
    private Block block;

    public Operation() {
    }

    public Operation(
            OperationId id,
            OperationType type,
            JSONObject data,
            Account sender,
            Account target
    ) {
        this.id = id;
        this.type = type;
        this.data = data;
        this.sender = sender;
        this.target = target;
    }

    public OperationId getId() {
        return id;
    }

    public OperationType getType() {
        return type;
    }

    public Operation setType(OperationType type) {
        this.type = type;
        return this;
    }

    public JSONObject getData() {
        return data;
    }

    public Operation setData(JSONObject data) {
        this.data = data;
        return this;
    }

    public Account getSender() {
        return sender;
    }

    public Operation setSender(Account sender) {
        this.sender = sender;
        return this;
    }

    public Account getTarget() {
        return target;
    }

    public Operation setTarget(Account target) {
        this.target = target;
        return this;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public Block getBlock() {
        return block;
    }
}
