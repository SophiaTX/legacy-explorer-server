package com.sophiatx.explorer.database.model;

import com.sophiatx.explorer.database.type.JSONObjectType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.json.JSONObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "json_storage")
@TypeDefs({
        @TypeDef(name = "JSONObject", typeClass = JSONObjectType.class)
})
public class JsonEntity {
    @Id
    @Column(nullable = false)
    private String key;

    @Type(type = "JSONObject")
    @Column(columnDefinition = "JSONB")
    private JSONObject data;

    public JsonEntity() {
    }

    public JsonEntity(String key, JSONObject data) {
        this.key = key;
        this.data = data;
    }

    public String getKey() {
        return key;
    }

    public JsonEntity setKey(String key) {
        this.key = key;
        return this;
    }

    public JSONObject getData() {
        return data;
    }

    public JsonEntity setData(JSONObject data) {
        this.data = data;
        return this;
    }
}
