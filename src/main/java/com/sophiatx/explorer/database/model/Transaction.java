package com.sophiatx.explorer.database.model;

import com.sophiatx.explorer.database.id.TransactionId;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "transactions")
public class Transaction {
    @EmbeddedId
    private TransactionId id;

    @Column(name = "ref_block_num")
    private Long refBlockNum;

    @Column(name = "ref_block_prefix")
    private Long refBlockPrefix;

    @ElementCollection(targetClass = String.class)
    private List<String> signatures;

    @OneToMany(
            mappedBy = "transaction",
            targetEntity = Operation.class,
            cascade = CascadeType.ALL
    )
    private List<Operation> operations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
            name = "block_height",
            referencedColumnName = "height",
            insertable = false,
            updatable = false
    )
    private Block block;

    public Transaction() {
        this.signatures = new ArrayList<>();
        this.operations = new ArrayList<>();
    }

    public Transaction(
            TransactionId id,
            Long refBlockNum,
            Long refBlockPrefix,
            List<String> signatures
    ) {
        this.id = id;
        this.refBlockNum = refBlockNum;
        this.refBlockPrefix = refBlockPrefix;
        this.signatures = signatures;
        this.operations = new ArrayList<>();
    }

    public TransactionId getId() {
        return id;
    }

    public Long getRefBlockNum() {
        return refBlockNum;
    }

    public Transaction setRefBlockNum(Long refBlockNum) {
        this.refBlockNum = refBlockNum;
        return this;
    }

    public Long getRefBlockPrefix() {
        return refBlockPrefix;
    }

    public Transaction setRefBlockPrefix(Long refBlockPrefix) {
        this.refBlockPrefix = refBlockPrefix;
        return this;
    }

    public List<String> getSignatures() {
        return signatures;
    }

    public Transaction setSignatures(List<String> signatures) {
        this.signatures = signatures;
        return this;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public Transaction addOperation(Operation operation) {
        operation.getId().setTransactionId(id);
        this.operations.add(operation);
        return this;
    }

    public Transaction setOperations(List<Operation> operations) {
        this.operations = operations;
        return this;
    }

    public Block getBlock() {
        return block;
    }
}
