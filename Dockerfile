FROM ubuntu:xenial
MAINTAINER Patrik Gajdos <patrik.gajdos@sophiatx.com>

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8

# Install necessary packages
RUN apt-get -y update && \
    apt-get -y install locales && \
    locale-gen en_US.UTF-8 && \
    update-locale LANG=en_US.UTF-8 && \
    apt-get -y install software-properties-common wget sudo git curl \
    openjdk-8-jre openjdk-8-jdk postgresql postgresql-contrib && \
    add-apt-repository -y ppa:cwchien/gradle && \
    curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash - && \
    apt-get -y update && \
    apt-get -y install gradle nodejs && \
    npm install -g @angular/cli --unsafe && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* /var/tmp/*

WORKDIR /app

# Configure db
RUN echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/9.5/main/pg_hba.conf && \
    echo "listen_addresses='*'" >> /etc/postgresql/9.5/main/postgresql.conf

# Create PG user & db ( password is irrelevant, db is not exposed )
RUN service postgresql start && \
    sudo -u postgres psql -c "CREATE USER sphtxexplorer WITH PASSWORD 'letmein'" && \
    sudo -u postgres createdb -E UTF8 -O sphtxexplorer sphtxexplorer && \
    service postgresql stop

# Copy source code to the container
COPY build.gradle explorer-server/build.gradle
COPY src          explorer-server/src/

# Clone & build web client and move the compiler output ( static source )
# to explorer server's static assets folder ( this will be served over HTTP )
ARG WEBCLIENT_VERSION
RUN if [ "$WEBCLIENT_VERSION" != "" ]; then \
    git clone -b ${WEBCLIENT_VERSION} https://gitlab.com/SophiaTX/legacy-explorer-webclient.git explorer-webclient && \
    cd explorer-webclient/ && \
    npm install && \
    ng build --prod --build-optimizer --aot && \
    mv dist/ ../explorer-server/src/main/resources/static/ && \
    cd ../ && rm -rf explorer-webclient/ ; \
    fi

# Build the explorer server containing webclient app in it
RUN cd explorer-server/ && \
    gradle build && \
    mv build/libs/explorer-server.jar ../explorer-server.jar && \
    rm -rf explorer-server/

# Create launch script
ARG NODE_HOST
ARG NODE_PORT
RUN touch start.sh && chmod +x start.sh && \
    echo "service postgresql start && java -jar explorer-server.jar \\" \
        >> start.sh \
    echo "--explorer.node.host=${NODE_HOST} --explorer.node.port=${NODE_PORT}" \
        >> start.sh

EXPOSE 8080

CMD ./start.sh
