#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root!"
    exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
    echo "Docker is not present on your system!"
    exit 1
fi

# Run in the detached mode with tty allocated and stdin left open
docker start sphtx-explorer-devstack
