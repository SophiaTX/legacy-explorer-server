#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root!"
    exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
    echo "Docker is not installed, nothing to remove."
    exit 1
fi

echo "Uninstalling Docker..."

apt-get purge docker-ce
rm -rf /var/lib/docker
