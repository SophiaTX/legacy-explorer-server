# Docker development container

This image contains working SophiaTX blockchain node and database, 
both of which are required for the development of this software.

Ports 8090 ( node ) and 5432 ( database ) are exposed by default. 
If you don't have these ports available, you can change the bindings in the 
`run.sh` script ( you will also have to change these in the 
 [application.properties](../../src/main/resources/application.properties) file).

To build the container, use `build.sh`.

To run the container, use `run.sh`.

To get rid of the container, use `clean.sh`.

#### Please note:

The explorer server itself should be compiled using Gradle build tool.

IntelliJ IDEA will handle compilation and runs for you. 
Just open it as a Gradle project.
