FROM ubuntu:xenial
MAINTAINER Patrik Gajdos <patrik.gajdos@sophiatx.com>

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8

RUN apt-get -y update -qq && \
    apt-get -y install locales && \
    locale-gen en_US.UTF-8 && \
    update-locale LANG=en_US.UTF-8 && \
    apt-get -y install qt5-default libqt5svg5-dev libreadline-dev \
    libcrypto++-dev libgmp-dev libdb-dev libdb++-dev libssl-dev \
    libncurses5-dev libboost-all-dev libcurl4-openssl-dev libicu-dev \
    libbz2-dev postgresql postgresql-contrib sudo tar && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /tmp/* /var/tmp/*

WORKDIR /node

# Configure db
RUN echo "host all  all    0.0.0.0/0  trust" >> /etc/postgresql/9.5/main/pg_hba.conf && \
    echo "listen_addresses='*'" >> /etc/postgresql/9.5/main/postgresql.conf

# Create PG user & db ( password is irrelevant, db is not exposed )
RUN service postgresql start && \
    sudo -u postgres psql -c "CREATE USER sphtxexplorer WITH PASSWORD 'letmein'" && \
    sudo -u postgres createdb -E UTF8 -O sphtxexplorer sphtxexplorer && \
    service postgresql stop

COPY sophiatxd.tar.gz sophiatxd.tar.gz
COPY sphtx-genesis.json sphtx-genesis.json
COPY config.ini /root/.sophiatx/data/sophiatxd/config.ini
RUN tar xvf sophiatxd.tar.gz

EXPOSE 5432
EXPOSE 8090

CMD service postgresql start && \
    /node/sophiatxd
