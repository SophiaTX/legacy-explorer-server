#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]; then
    echo "This script must be run as root!"
    exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
    echo "Docker is not present on your system!"
    exit 1
fi

# We are getting rid of the container, killing it is faster
docker kill sphtx-explorer-devstack
docker rm sphtx-explorer-devstack