#!/usr/bin/env bash

trap "exit 1" TERM
export TOP_PID=$$

BRAND="SophiaTX Blockchain Explorer"
BACKTITLE="${BRAND} / production deployment"
NODE_CONTAINER_NAME="explonode"
NODE_DEFAULT_PORT=8090
EXPLORER_CONTAINER_NAME="exploserver"
EXPLORER_DEFAULT_PORT=80
NET_NAME="explonet"
DEFAULT_WEBCLIENT_VERSION="master"

function abort {
    echo "Aborting deployment.">&2
    kill -s TERM ${TOP_PID}
}

function verify_whiptail_availability {
    if ! [ -x "$(command -v whiptail)" ]; then
        echo "Package whiptail is not available, please confirm its installation."
        exec sudo apt-get -y install whiptail
    fi
}

function show_welcome_message {
    whiptail --backtitle "$BACKTITLE" \
             --title "$BRAND Installer" \
             --yesno "Welcome to $BRAND installer. This script will help you \
deploy the app and configure its components. Your inputs are NOT validated so \
make sure what you enter is correct. If you want to cancel the installation, \
you can do so by pressing ESC at any time." \
             --yes-button "Continue" \
             --no-button "Abort" \
             12 60
    local exitstatus=$?
    if [ ${exitstatus} -ne 0 ]; then abort; fi
}

function verify_root_access {
    if [ $EUID -ne 0 ]; then
        sudo -k
        pass=$(whiptail \
                --backtitle "$BACKTITLE" \
                --title "Authentication required" \
                --passwordbox "Deployment of $BRAND requires administrator \
privileges to use Docker containers.\n\n[sudo] Password for $USER:" \
                12 60 3>&1 1>&2 2>&3 \
        )

        if [ $? -ne 0 ]; then abort; fi;

        exec sudo -S -p '' "$0" "$@" <<< "$pass"
        unset pass
    fi
}

function verify_docker_installation {
    if ! [ -x "$(command -v docker)" ]; then
        whiptail --backtitle "$BACKTITLE" \
                 --title "Docker not found" \
                 --yesno "Docker is not installed on your system but is \
required for the deployment. Would you like to install it now?" \
                 8 60 \
                 3>&1 1>&2 2>&3

        if [ $? -eq 0 ]; then install_docker; else abort; fi;
    fi
}

function install_docker {
    echo "Installing Docker..."

    apt-get update
    apt-get install -y \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common

    # Add Docker's official GPG key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    # Add Docker repo
    sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) /
        stable"
    apt-get update

    # Install docker
    apt-get install -y docker-ce
}

function ask_node_type {
    whiptail --nocancel \
             --backtitle "$BACKTITLE" \
             --title "Blockchain node setup" \
             --menu "How would you like to access SophiaTX blockchain?" \
             11 60 3 \
             "internal" "Deploy internal blockchain node" \
             "local"    "Use local blockchain node" \
             "remote"   "Use remote blockchain node" \
             3>&1 1>&2 2>&3

    if [ $? -ne 0 ]; then abort; fi;
}

function ask_web_client {
    whiptail --backtitle "$BACKTITLE" \
             --title "Explorer web client" \
             --yesno "Would you like to include web client for the explorer?" \
             8 60 \
             3>&1 1>&2 2>&3
    local exitstatus=$?

    if [ ${exitstatus} -eq 255 ]; then abort; fi
    if [ ${exitstatus} -eq 0 ]; then
        echo 1
    else
        echo 0
    fi
}

function ask_explorer_webclient_version {
    whiptail --nocancel \
             --backtitle "$BACKTITLE" \
             --title "Explorer web client" \
             --inputbox "Enter a tag or branch to deploy. Leave blank to cancel." \
             9 60 ${DEFAULT_WEBCLIENT_VERSION} \
             3>&1 1>&2 2>&3

    if [ $? -ne 0 ]; then abort; fi;
}

function ask_explorer_port {
    whiptail --nocancel \
             --backtitle "$BACKTITLE" \
             --title "Explorer configuration" \
             --inputbox "Which PORT would you like the explorer to listen on?" \
             9 60 ${EXPLORER_DEFAULT_PORT} \
             3>&1 1>&2 2>&3

    if [ $? -ne 0 ]; then abort; fi;
}

function ask_node_host {
    whiptail --nocancel \
             --backtitle "$BACKTITLE" \
             --title "Node configuration" \
             --inputbox "Enter node HOST" \
             9 60 \
             3>&1 1>&2 2>&3

    if [ $? -ne 0 ]; then abort; fi;
}

function ask_node_port {
    whiptail --nocancel \
             --backtitle "$BACKTITLE" \
             --title "Node configuration" \
             --inputbox "Enter node PORT" \
             9 60 ${NODE_DEFAULT_PORT} \
             3>&1 1>&2 2>&3

    if [ $? -ne 0 ]; then abort; fi;
}

verify_whiptail_availability
verify_root_access
show_welcome_message
verify_docker_installation
explorer_port=$(ask_explorer_port)

include_web_client=$(ask_web_client)
if [ ${include_web_client} -eq 1 ]; then
    webclient_version=$(ask_explorer_webclient_version)
fi

node_type=$(ask_node_type)

if [ ${node_type} == "internal" ]; then
    node_host=${NODE_CONTAINER_NAME}
    node_port=${NODE_DEFAULT_PORT}
fi

if [ ${node_type} == "local" ]; then
    # TODO: this can not be accessed from inside of container
    node_host="localhost"
    node_port=$(ask_node_port)
fi

if [ ${node_type} == "remote" ]; then
    node_host=$(ask_node_host)
    node_port=$(ask_node_port)
fi

if [ "$node_host" == "localhost" ] || [ "$node_host" == "127.0.0.1" ]; then
    whiptail --backtitle "$BACKTITLE" \
             --title "Not supported yet" \
             --msgbox "Sorry, local nodes are not supported yet due to issues \
with networking between host machine and explorer's Docker container. This \
will eventually get resolved. Until then, please use internal or remote node. \
You can even point \"remote\" node to a node installed locally using server's \
public IP address." \
             12 70
    exit 1;
fi

if [ ${include_web_client} -eq 1 ];
    then incl="YES";
    else incl="NO";
fi

whiptail --backtitle "$BACKTITLE" \
         --title "Installation summary" \
         --msgbox "Listen on port: $explorer_port

Include web client: $incl
Web client version: $webclient_version

Node type: $node_type
Node host: $node_host
Node port: $node_port" \
         15 70

if [ ${node_type} == "internal" ]; then
    cd node/
    docker network create --driver=bridge ${NET_NAME}
    docker build -t ${NODE_CONTAINER_NAME} .
    docker run -d -ti --restart=always --net=${NET_NAME} \
            --name ${NODE_CONTAINER_NAME} ${NODE_CONTAINER_NAME}
    cd ../
fi

cd ../../
if [ ${include_web_client} -eq 1 ]; then
    docker build -t ${EXPLORER_CONTAINER_NAME} \
            --build-arg NODE_HOST="$node_host" \
            --build-arg NODE_PORT="$node_port" \
            --build-arg WEBCLIENT_VERSION="$webclient_version"
else
    docker build -t ${EXPLORER_CONTAINER_NAME} \
            --build-arg NODE_HOST="$node_host" \
            --build-arg NODE_PORT="$node_port"
fi

if [ ${node_type} == "internal" ]; then
    docker run -d -ti -p ${explorer_port}:8080 --restart=always \
            --net=${NET_NAME} \
            --name ${EXPLORER_CONTAINER_NAME} ${EXPLORER_CONTAINER_NAME}
else
    docker run -d -ti -p ${explorer_port}:8080 --restart=always \
            --name ${EXPLORER_CONTAINER_NAME} ${EXPLORER_CONTAINER_NAME}
fi
