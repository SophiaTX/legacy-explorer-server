# SophiaTX Blockchain Explorer Deployment

Installation wizard will help you deploy the explorer software stack.

Run `deploy.sh` and follow the instructions on the screen. 

Whole deployment process is fully automated and may take a while.

### Web client as a part of explorer server

If you want the web client to be served using explorer's built-in HTTP server,
just confirm this option when requested by the deploy script and provide your
GitLab access token with "read_repository" permission ( used to pull the repo ).

### Node selection

There are two options available.

1 \) Let the installer deploy internal node alongside explorer in
its own container that is not accessible from outside but is connected to the
main explorer container via docker network. This is the recommended option.

2 \) Use a node that is already installed on the same machine and listens on
localhost ( 127.0.0.1 ). Useful in case you already deployed a standalone node
and the machine before and its not accessible from outside.

3 \) Use a node that is already installed remotely on a different machine. 
This is not recommended as it may cause some delay and initial import will be 
slow. Node must listen on its public address ( which you will enter in the 
next step ) and the port has to be open on the firewall.

## Remove stack

To stop and remove all the explorer's containers, you can use `clean.sh`.