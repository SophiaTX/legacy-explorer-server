#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]; then
  echo "This script must be run as root!"
  exit
fi

echo "Removing SophiaTX Blockchain Explorer stack..."

# Remove explorer server
docker kill exploserver
docker rm exploserver

# Remove SophiaTX node
docker kill explonode
docker rm explonode

# Remove bridge network
docker network rm explonet
